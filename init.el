;;
;;; init.el --- Prelude's configuration entry point.
;; Copyright (c) 2011 Bozhidar Batsov
;;
;; Author: Bozhidar Batsov <bozhidar@batsov.com>
;; URL: http://batsov.com/prelude
;; Version: 1.0.0
;; Keywords: convenience

;; This file is not part of GNU Emacs.

;;; Commentary:

;; This file simply sets up the default load path and requires
;; the various modules defined within Emacs Prelude.

;;; License:

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 3
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Code:
(setq *emacs-dir* (concat (getenv "HOME") "/.emacs1.d"))
(load (concat *emacs-dir* "/emacs-rc-cedet.el"))
(let ((benchmark-init (concat *emacs-dir* "/elpa/benchmark-init-20140510.732/benchmark-init.el")))
  (when (file-exists-p benchmark-init)
    (load benchmark-init)))
(require 'benchmark-init)

(defvar current-user
  (getenv
   (if (equal system-type 'windows-nt) "USERNAME" "USER")))

(message "Prelude is powering up... Be patient, Master %s!" current-user)

(when (version< emacs-version "24.1")
  (error "Prelude requires at least GNU Emacs 24.1, but you're running %s" emacs-version))

;; Always load newest byte code
(setq load-prefer-newer t)

(defvar prelude-dir (file-name-directory load-file-name)
  "The root dir of the Emacs Prelude distribution.")
(defvar prelude-core-dir (expand-file-name "core" prelude-dir)
  "The home of Prelude's core functionality.")
(defvar prelude-modules-dir (expand-file-name  "modules" prelude-dir)
  "This directory houses all of the built-in Prelude modules.")
(defvar prelude-personal-dir (expand-file-name "personal" prelude-dir)
  "This directory is for your personal configuration.

Users of Emacs Prelude are encouraged to keep their personal configuration
changes in this directory.  All Emacs Lisp files there are loaded automatically
by Prelude.")
(defvar prelude-personal-preload-dir (expand-file-name "preload" prelude-personal-dir)
  "This directory is for your personal configuration, that you want loaded before Prelude.")
(defvar prelude-vendor-dir (expand-file-name "vendor" prelude-dir)
  "This directory houses packages that are not yet available in ELPA (or MELPA).")
(defvar prelude-savefile-dir (expand-file-name "savefile" prelude-dir)
  "This folder stores all the automatically generated save/history-files.")
(defvar prelude-modules-file (expand-file-name "prelude-modules.el" prelude-dir)
  "This files contains a list of modules that will be loaded by Prelude.")

(unless (file-exists-p prelude-savefile-dir)
  (make-directory prelude-savefile-dir))

(defun prelude-add-subfolders-to-load-path (parent-dir)
  "Add all level PARENT-DIR subdirs to the `load-path'."
  (dolist (f (directory-files parent-dir))
    (let ((name (expand-file-name f parent-dir)))
      (when (and (file-directory-p name)
                 (not (string-prefix-p "." f)))
        (add-to-list 'load-path name)
        (prelude-add-subfolders-to-load-path name)))))

;; add Prelude's directories to Emacs's `load-path'
(add-to-list 'load-path prelude-core-dir)
(add-to-list 'load-path prelude-modules-dir)
(add-to-list 'load-path prelude-vendor-dir)
(prelude-add-subfolders-to-load-path prelude-vendor-dir)

;; reduce the frequency of garbage collection by making it happen on
;; each 50MB of allocated data (the default is on every 0.76MB)
(setq gc-cons-threshold 50000000)

;; preload the personal settings from `prelude-personal-preload-dir'
(when (file-exists-p prelude-personal-preload-dir)
  (message "Loading personal configuration files in %s..." prelude-personal-preload-dir)
  (mapc 'load (directory-files prelude-personal-preload-dir 't "^[^#].*el$")))

(message "Loading Prelude's core...")

;; the core stuff
(require 'prelude-packages)
(require 'prelude-ui)
(require 'prelude-custom)
(require 'prelude-core)
(require 'prelude-mode)
(require 'prelude-editor)
(require 'prelude-global-keybindings)

;; OSX specific settings
(when (eq system-type 'darwin)
  (require 'prelude-osx))

(message "Loading Prelude's modules...")

;; the modules
(when (file-exists-p prelude-modules-file)
  (load prelude-modules-file))

;; config changes made through the customize UI will be store here
(setq custom-file (expand-file-name "custom.el" prelude-personal-dir))

;; load the personal settings (this includes `custom-file')
(when (file-exists-p prelude-personal-dir)
  (message "Loading personal configuration files in %s..." prelude-personal-dir)
  (mapc 'load (directory-files prelude-personal-dir 't "^[^#].*el$")))

(require 'prelude-programming)
(require 'prelude-c)
;; (require 'prelude-clojure)
;; (require 'prelude-coffee)
(require 'prelude-common-lisp)
;; (require 'prelude-css)
(require 'prelude-emacs-lisp)
(require 'prelude-erc)
(require 'prelude-erlang)
;; (require 'prelude-haskell)
(require 'prelude-js)
;; (require 'prelude-latex)
(require 'prelude-lisp)
;; (require 'prelude-markdown)
;; (require 'prelude-mediawiki)
;; (require 'prelude-ido)
(require 'prelude-org)
(require 'prelude-perl)
(require 'prelude-python)
;; (require 'prelude-ruby)
;; (require 'prelude-scala)
(require 'prelude-scheme)
;; (require 'prelude-scss)
;; (require 'prelude-web)
;; (require 'prelude-helm)
(require 'prelude-xml)
(require 'prelude-key-chord)

;; (require 'man-commands)
;; (require 'ox-reveal)
(require 'prelude-mode)

(add-to-list 'package-archives
             '("marmalade" .
               "http://marmalade-repo.org/packages/"))
(setq package-archives (cons '("tromey" . "http://tromey.com/elpa/") package-archives))
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/") t)
(package-initialize)

(defadvice kill-region (before slick-cut activate compile)
  "When called interactively with no active region, kill a single
line instead."
  (interactive
   (if mark-active (list (region-beginning) (region-end))
     (list (line-beginning-position)
           (line-beginning-position 2)))))

(defadvice kill-buffer (around kill-buffer-around-advice activate)
  (let ((buffer-to-kill (ad-get-arg 0)))
    (if (equal buffer-to-kill "*scratch*")
        (bury-buffer)
      ad-do-it)))

(defadvice kill-line (before check-position activate)
  (if (member major-mode
              '(emacs-lisp-mode scheme-mode lisp-mode
                                c-mode c++-mode objc-mode
                                latex-mode plain-tex-mode))
      (if (and (eolp) (not (bolp)))
          (progn (forward-char 1)
                 (just-one-space 0)
                 (backward-char 1)))))

(defun my-create-newline-and-enter-sexp (&rest _ignored)
  "Open a new brace or bracket expression, with relevant newlines and indent. "
  (newline)
  (indent-according-to-mode)
  (forward-line -1)
  (indent-according-to-mode))

(sp-local-pair 'c-mode "{" nil :post-handlers '((my-create-newline-and-enter-sexp "RET")))
(sp-local-pair 'c++-mode "{" nil :post-handlers '((my-create-newline-and-enter-sexp "RET")))

(require 'info+)

(setq prelude-guru nil)
(setq confirm-nonexistent-file-or-buffer nil)
(setq ace-jump-mode-gray-background nil)
(setq prelude-whitespace nil)
(setq
 inhibit-startup-message t
 backup-directory-alist `((".*" . ,temporary-file-directory)) ;don't clutter my fs and put backups into tmp
 auto-save-file-name-transforms `((".*" ,temporary-file-directory t))
 require-final-newline t                ;auto add newline at the end of file
 column-number-mode t                   ;show the column number
 default-major-mode 'text-mode          ;use text mode per default
 mouse-yank-at-point t                  ;middle click with the mouse yanks at point
 history-length 250                     ;default is 30
 locale-coding-system 'utf-8            ;utf-8 is default
 tab-always-indent 'complete            ;try to complete before identing
 confirm-nonexistent-file-or-buffer nil ;don't ask to create a buffer
 vc-follow-symlinks t                   ;follow symlinks automatically
 recentf-max-saved-items 5000           ;same up to 5000 recent files
 eval-expression-print-length nil       ;do not truncate printed expressions
 eval-expression-print-level nil        ;print nested expressions
 send-mail-function 'sendmail-send-it
 kill-ring-max 5000                     ;truncate kill ring after 5000 entries
 mark-ring-max 5000                     ;truncate mark ring after 5000 entries
 ;; mouse-autoselect-window -.1            ;window focus follows the mouse pointer
 mouse-wheel-scroll-amount '(1 ((shift) . 5) ((control))) ;make mouse scrolling smooth
 indicate-buffer-boundaries 'left       ;fringe markers
 split-height-threshold 110             ;more readily split horziontally
 enable-recursive-minibuffers t
 custom-unlispify-menu-entries nil      ;M-x customize should not cripple menu entries
 custom-unlispify-tag-names nil         ;M-x customize should not cripple tags
 show-paren-delay 0
 view-read-only t
 )

(setq user-mail-address "tuhdo1710@gmail.com")
(setq user-full-name "Tu, Do Hoang")
(setq smtpmail-smtp-server "mail.google.com")
(setq mail-user-agent 'message-user-agent)
(setq message-send-mail-function 'message-smtpmail-send-it)

;; (set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-language-environment "UTF-8")
(prefer-coding-system 'utf-8)

(define-key global-map (kbd "RET") 'newline-and-indent)

(case window-system
  ((x w32) (progn
             (scroll-bar-mode -1)
             (nyan-mode))))

;; (global-whitespace-mode)
(electric-indent-mode +1)

;; (load "~/.emacs.d/personal/rudel-bzr/rudel-loaddefs.el")
;; (global-rudel-minor-mode)

;; (require 'latex-pretty-symbols)
;; (eval-after-load 'latex '(latex/setup-keybinds))

;; (global-rainbow-delimiters-mode)
(global-diff-hl-mode)

(require 'morlock)
(font-lock-add-keywords 'emacs-lisp-mode morlock-el-font-lock-keywords)
(font-lock-add-keywords 'emacs-lisp-mode morlock-cl-font-lock-keywords)

(eval-after-load "thingatpt"
  '(require 'thingatpt+))

(setq prelude-flyspell nil)

;;kill whole line with ctrl-k
(setq kill-whole-line t)

;;paren mode
(require 'paren)
(show-paren-mode t)

;;excaburant ctags
(setq path-to-ctags "/usr/local/bin/ctags") ;; <- your ctags path here
;;load cedet builtin config
                                        ;(load "~/.emacs.d/emacs-cedet-builtint")
(defun create-tags (dir-name)
  "Create tags file."
  (interactive "DDirectory: ")
  (shell-command
   (format "%s -f %s/TAGS -e -R %s" path-to-ctags dir-name (directory-file-name dir-name))))

;; cursor and mouse
(setq comment-auto-fill-only-comments t)
;; (add-hook 'text-mode-hook 'turn-on-auto-fill)
(add-hook 'git-commit-mode-hook 'turn-on-auto-fill)
(add-hook 'org-mode-hook 'turn-on-auto-fill)

;; (setq-default indent-tabs-mode nil)    ; use only spaces and no tabs
;; (setq default-tab-width 4)

;; (yas-global-mode 1)

;;mini-map
;; (require 'minimap)

(setq shell-prompt-pattern " ")
(setq tramp-chunksize 500)

(add-hook 'emacs-lisp-mode-hook 'turn-on-eldoc-mode)
(add-hook 'lisp-interaction-mode-hook 'turn-on-eldoc-mode)
(add-hook 'ielm-mode-hook 'turn-on-eldoc-mode)

;;tramp pattern
;; (setq tramp-shell-prompt-pattern "^[^$>\n]*[#$%>] *\\(\[[0-9;]*[a-zA-Z] *\\)*")

;;ssh-config-mode
(autoload 'ssh-config-mode "ssh-config-mode" t)
(add-to-list 'auto-mode-alist '(".ssh/config\\'"  . ssh-config-mode))
(add-to-list 'auto-mode-alist '("sshd?_config\\'" . ssh-config-mode))
(add-hook 'ssh-config-mode-hook 'turn-on-font-lock)

;;expand-region
(require 'expand-region)

;; when using ido, the confirmation is rather annoying...
(setq confirm-nonexistent-file-or-buffer nil)

;; increase minibuffer size when ido completion is active
(add-hook 'ido-minibuffer-setup-hook
          (function
           (lambda ()
             (make-local-variable 'resize-minibuffer-window-max-height)
             (setq resize-minibuffer-window-max-height 1))))

;;kill buffer on pane
(defun kill-next-pane ()
  "Kill the next buffer in it also."
  (interactive)
  (other-window 1)
  (kill-buffer-and-window)
  (other-window 1))

;;buf-move
;; (require 'windmove)
(windmove-default-keybindings)

;; (set-frame-font "Inconsolata-11")
(setq default-frame-alist '((font . "Inconsolata-11")))

;;shell colors
(add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)

;;shell-pop
;; (require 'shell-pop)
;; (global-set-key [f12] 'shell-pop)

(dolist (hook '(emacs-lisp-mode-hook ielm-mode-hook))
  (add-hook hook 'turn-on-elisp-slime-nav-mode))

;; (require 'replace+)
;; (require 'doremi)
(require 'hide-mode-line)
(require 'setup-auto-capitalize)
;; (require 'setup-automark)
(require 'setup-bookmark)
;; (require 'setup-company)
(require 'setup-desktop)
(require 'setup-dired)
(require 'setup-deft)
(require 'setup-dictionary)
(require 'setup-etags-utilities)
(require 'setup-eww)
(require 'setup-flycheck)
(require 'setup-flyspell) ;; must place before evilnc
(require 'setup-linum) ;; must place before evilnc
;; (require 'setup-fuzzy-format)
;; (require 'setup-godmode)
(require 'setup-helm)
(require 'setup-helm-gtags)
(require 'setup-hippie-expand)
(require 'setup-programming-modes) ;; must be placed after setup-helm to activate helm-interface for moo-complete
;; (require 'setup-highlight-parentheses)
(require 'setup-highlight-symbol)
;; (require 'setup-ido)
(require 'setup-golden-ratio)
(require 'setup-irfc)
(require 'setup-magit)
;; (require 'setup-misc-cmds)
(require 'setup-mc-and-cua)
;; (require 'setup-nlinum)
(require 'setup-org)
;; (require 'setup-org-present)
(require 'setup-rainbow-delimiters)
(require 'setup-synonyms)
(require 'setup-screenshot)
(require 'setup-sexp-fu)
;; (require 'setup-smart-op)
;; (require 'setup-smart-tabs)
;; (require 'setup-xcscope)
(require 'setup-yasnippet)
(require 'setup-popwin)
;; (require 'setup-guide-key)
(require 'setup-term)
(require 'setup-quickrun)
(require 'syslog-mode)
(require 'yas-oneshot)
(require 'setup-gnus)
(require 'setup-ac-math)
(require 'setup-base-converter)
(require 'setup-pager)

;; (global-set-key (kbd "M-x")
;;                 '(lambda (prefix) (interactive "P")
;;                    (cond ((fboundp 'smex) (smex))
;;                          ((fboundp 'helm-M-x) (helm-M-x))
;;                          ((execute-extended-command prefix)))))

;; (require 'simple-mode-line)
;; (activate-simple-mode-line)

(setq epa-file-cache-passphrase-for-symmetric-encryption t)

(setq sp-autoescape-string-quote nil)

;; (global-set-key (kbd "<f8>") 'smex)
;; (global-set-key (kbd "<f7>") 'smex-major-mode-commands)

;; (defun copy-line (arg)
;;   "Copy lines (as many as prefix argument) in the kill ring"
;;   (interactive "p")
;;   (kill-ring-save (line-beginning-position)
;;                   (line-beginning-position (+ 1 arg)))
;;   (message "%d line%s copied" arg (if (= 1 arg) "" "s")))

;; (global-set-key "\C-c\C-w" 'copy-line)

(global-set-key [f1] 'help-on-click/key)

;;enable-flex-isearch
(if (not (and (boundp 'flex-isearch-mode) flex-isearch-mode))
    (flex-isearch-mode))

;;dedicated window
(defun toggle-current-window-dedication ()
  (interactive)
  (let* ((window    (selected-window))
         (dedicated (window-dedicated-p window)))
    (set-window-dedicated-p window (not dedicated))
    (message "Window %sdedicated to %s"
             (if dedicated "no longer " "")
             (buffer-name))))
;; Toggle window dedication

(require 'occur-x)
(add-hook 'occur-mode-hook 'turn-on-occur-x-mode)
(substitute-key-definition 'query-replace 'query-replace-w-options
                           global-map)

;;edit file as sudo
(defun sudo-edit (&optional arg)
  "Edit currently visited file as root.

With a prefix ARG prompt for a file to visit.
Will also prompt for a file to visit if current
buffer is not visiting a file."
  (interactive "P")
  (if (or arg (not buffer-file-name))
      (find-file (concat "/sudo:root@localhost:"
                         (ido-read-file-name "Find file(as root): ")))
    (find-alternate-file (concat "/sudo:root@localhost:" buffer-file-name))))

(global-set-key [Scroll_Lock] 'toggle-current-window-dedication)

(setq view-read-only t)

(defun kill-default-buffer ()
  "Kill the currently active buffer -- set to C-x k so that users are not asked which buffer they want to kill."
  (interactive)
  (let (kill-buffer-query-functions) (kill-buffer)))

(defun my-switch-to-other-buffer ()
  "Switch to other buffer"
  (interactive)
  (switch-to-buffer (other-buffer)))

(require 'linum-off)
(global-linum-mode)

;; (require 'iedit)

;; (setq sml/theme 'dark)
;; (require 'smart-mode-line)
;; (sml/setup)

;; (require 'powerline)
;; (powerline-default-theme)

(require 'perspective)
(persp-mode)
(require 'register-list)

;; (global-set-key (kbd "C-\\ g") 'spl-grid)

(require 'tempbuf)
(add-hook 'custom-mode-hook 'turn-on-tempbuf-mode)
(add-hook 'w3-mode-hook 'turn-on-tempbuf-mode)
;; (add-hook 'Man-mode-hook 'turn-on-tempbuf-mode)
;; (add-hook 'view-mode-hook 'turn-on-tempbuf-mode)

;; (load-theme 'twlight t)
(color-theme-sanityinc-tomorrow-bright)
;; (defadvice linum-update-window (around linum-dynamic activate)
;;   (let* ((w (length (number-to-string
;;                      (count-lines (point-min) (point-max)))))
;;          (linum-format (concat " %" (number-to-string w) "d ")))
;;     ad-do-it))

;; (load-theme 'grandshell t)
(require 'smartrep)
(require 'smart-window)
;; (require 'back-button)
;; (back-button-mode 1)

;; (require 'autopair)
;; (autopair-global-mode) ;; to enable in all buffers

;;par-edit
;; (require 'paredit)
;; (add-hook 'slime-repl-mode-hook (lambda () (paredit-mode)))
;; (add-hook 'emacs-lisp-mode-hook       (lambda () (paredit-mode)))
;; (add-hook 'lisp-mode-hook             (lambda () (paredit-mode)))
;; (add-hook 'lisp-interaction-mode-hook (lambda () (paredit-mode)))
;; (add-hook 'scheme-mode-hook           (lambda () (paredit-mode)))

;; (defadvice paredit-mode (around disable-autopairs-around (arg))
;;   "Disable autopairs mode if paredit-mode is turned on"
;;   ad-do-it
;;   (if (null ad-return-value)
;;       (autopair-mode 1)
;;     (autopair-mode 0)))
;; (ad-activate 'paredit-mode)

;; (require 'paredit-everywhere)
;; (add-hook 'tcl-mode-hook 'paredit-everywhere-mode)
;; (add-hook 'c-mode-hook 'paredit-everywhere-mode)
;; (add-hook 'c++-mode-hook 'paredit-everywhere-mode)

(require 'pretty-lambdada)
(pretty-lambda-for-modes)

(require 'sys-apropos)

(require 'auto-complete)
(require 'auto-complete-config)
(ac-config-default)

(add-hook 'after-init-hook 'global-company-mode)

(autoload 'multi-select-mode "multi-select" nil t)

(multi-select-mode t)

;; (require 'auto-complete-auctex)

(require 'command-log-mode)
;; (add-hook 'LaTeX-mode-hook 'command-log-mode)
(command-log-mode)

(require 'dedicated)

(require 'zop-to-char)

(global-auto-revert-mode 1)

(add-hook 'yaml-mode-hook
          (lambda () (set (make-local-variable 'electric-indent-mode) nil)))

(add-hook 'prog-mode-hook (lambda () (interactive) (setq show-trailing-whitespace 1)))

(add-hook 'lisp-mode-hook (lambda () (semantic-idle-summary-mode -1)))
;; (add-hook 'c-mode-hook (lambda () (semantic-idle-summary-mode 1)))

;; add hs-minor-mode for C/C++/Java
(add-hook 'c-mode-common-hook #'hs-minor-mode)

(add-hook 'git-commit-mode-hook 'fci-mode)

(require 'vlf-integrate)

(load (concat *emacs-dir* "/setup-modeline.el"))
(load (concat *emacs-dir* "/key-bindings.el"))

(ipretty-mode t)

(helm-mode t)

(setq-local eldoc-documentation-function #'ggtags-eldoc-function)
(add-hook 'tcl-mode-hook 'ggtags-mode)
(add-hook 'tcl-mode-hook 'eldoc-mode)
(add-hook 'tcl-mode-hook (lambda () (setq-local eldoc-documentation-function #'ggtags-eldoc-function)))

(ggtags-mode)

(require 'calfw)
(require 'calfw-org)

(require 'persp-projectile)
(define-key projectile-mode-map (kbd "C-c p s") 'projectile-persp-switch-project)

(require 'easy-kill)
;; (define-key easy-kill-base-map "\r" 'easy-kill-done)
;; (put 'easy-kill-done 'easy-kill-exit t)
;; (defalias 'easy-kill-done 'ignore)

(require 'ace-link)
(ace-link-setup-default)

(require 'visual-regexp)

(require 'google-translate)
(require 'google-translate-smooth-ui)
(setq google-translate-translation-directions-alist '(("en" . "vi") ("vi" . "en")))

(require 'discover-my-major)

(add-hook 'prog-mode 'hl-line-mode)

(add-hook 'prog-mode-hook 'number-font-lock-mode)
(add-hook 'compilation-mode-hook 'next-error-follow-minor-mode)

(setq mouse-wheel-scroll-amount
      '(1
        ((shift) . 5)
        ((control))))

;; (require 'win-switch)
;; (win-switch-setup-keys-ijkl "\C-xo" "\C-x\C-o")

(require 'recentf-ext)
(require 'buffer-move)

(require 'iflipb)
(global-set-key (kbd "M-<left>") 'iflipb-previous-buffer)
(global-set-key (kbd "M-<right>") 'iflipb-next-buffer)

(require 'company-c-headers)
(add-to-list 'company-backends 'company-c-headers)

(require 'smart-shift)

(global-smart-shift-mode 1)
(add-hook 'prog-mode-hook 'smartscan-mode)
(add-hook 'org-mode-hook 'smartscan-mode)

(require 'narrow-indirect)
(global-set-key (kbd "C-x n d") 'ni-narrow-to-defun-other-window)
(global-set-key (kbd "C-x n r") 'ni-narrow-to-region-other-window)
(global-set-key (kbd "C-x n p") 'ni-narrow-to-page-indirect-other-window)

(require 'clean-aindent-mode)
(add-hook 'prog-mode-hook 'clean-aindent-mode)

(message "Prelude is ready to do thy bidding, Master %s!" current-user)

;;; init.el ends here
