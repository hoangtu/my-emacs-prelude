;;; magit-filenotify-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (magit-filenotify-mode) "magit-filenotify" "magit-filenotify.el"
;;;;;;  (21323 17940 437320 948000))
;;; Generated autoloads from magit-filenotify.el

(autoload 'magit-filenotify-mode "magit-filenotify" "\
Refresh status buffer if source tree changes.

\(fn &optional ARG)" t nil)

;;;***

;;;### (autoloads nil nil ("magit-filenotify-pkg.el") (21323 17940
;;;;;;  558453 884000))

;;;***

(provide 'magit-filenotify-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; magit-filenotify-autoloads.el ends here
