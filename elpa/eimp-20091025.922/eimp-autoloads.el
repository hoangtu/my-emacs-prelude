;;; eimp-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (eimp-mode) "eimp" "eimp.el" (21147 24678 599992
;;;;;;  72000))
;;; Generated autoloads from eimp.el

(autoload 'eimp-mode "eimp" "\
Toggle Eimp mode.

\(fn &optional ARG)" t nil)

;;;***

;;;### (autoloads nil nil ("eimp-pkg.el") (21147 24678 705125 421000))

;;;***

(provide 'eimp-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; eimp-autoloads.el ends here
