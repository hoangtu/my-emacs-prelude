;;; helm-make-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (helm-make) "helm-make" "helm-make.el" (21270 43438
;;;;;;  578751 904000))
;;; Generated autoloads from helm-make.el

(autoload 'helm-make "helm-make" "\
Use `helm' to select a Makefile target and `compile'.

\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("helm-make-pkg.el") (21270 43438 690076
;;;;;;  390000))

;;;***

(provide 'helm-make-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; helm-make-autoloads.el ends here
