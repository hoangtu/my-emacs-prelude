;;; ace-link-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (or (file-name-directory #$) (car load-path)))

;;;### (autoloads nil "ace-link" "ace-link.el" (21351 28613 587352
;;;;;;  240000))
;;; Generated autoloads from ace-link.el

(autoload 'ace-link-info "ace-link" "\
Ace jump to links in `Info-mode' buffers.

\(fn)" t nil)

(autoload 'ace-link-help "ace-link" "\
Ace jump to links in `help-mode' buffers.

\(fn)" t nil)

(autoload 'ace-link-org "ace-link" "\
Ace jump to links in `org-mode' buffers.

\(fn)" t nil)

(autoload 'ace-link-setup-default "ace-link" "\
Setup the defualt shortcuts.

\(fn)" nil nil)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; ace-link-autoloads.el ends here
