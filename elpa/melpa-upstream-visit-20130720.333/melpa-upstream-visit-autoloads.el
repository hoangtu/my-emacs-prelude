;;; melpa-upstream-visit-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (muv) "melpa-upstream-visit" "melpa-upstream-visit.el"
;;;;;;  (21152 14448 440199 712000))
;;; Generated autoloads from melpa-upstream-visit.el

(autoload 'muv "melpa-upstream-visit" "\
`browse-url's (or at least tries to) the PACKAGE-NAME's homepage.

\(fn PACKAGE-NAME)" t nil)

;;;***

;;;### (autoloads nil nil ("melpa-upstream-visit-pkg.el") (21152
;;;;;;  14448 550454 703000))

;;;***

(provide 'melpa-upstream-visit-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; melpa-upstream-visit-autoloads.el ends here
