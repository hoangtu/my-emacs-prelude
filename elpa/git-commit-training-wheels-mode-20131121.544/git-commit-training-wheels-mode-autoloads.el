;;; git-commit-training-wheels-mode-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (git-commit-training-wheels-mode) "git-commit-training-wheels-mode"
;;;;;;  "git-commit-training-wheels-mode.el" (21156 40603 348425
;;;;;;  831000))
;;; Generated autoloads from git-commit-training-wheels-mode.el

(autoload 'git-commit-training-wheels-mode "git-commit-training-wheels-mode" "\
Helps you craft well formed commit messages with `git-commit-mode'.

\(fn &optional ARG)" t nil)

;;;***

;;;### (autoloads nil nil ("git-commit-training-wheels-mode-pkg.el")
;;;;;;  (21156 40603 442054 978000))

;;;***

(provide 'git-commit-training-wheels-mode-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; git-commit-training-wheels-mode-autoloads.el ends here
