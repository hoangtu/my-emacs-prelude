(define-package "anzu" "20140422.1934" "Show number of matches in mode-line while searching" '((cl-lib "0.5") (emacs "24")) :url "https://github.com/syohex/emacs-anzu")
