;;; misc-fns-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (mode-line-reminder-duration) "misc-fns" "misc-fns.el"
;;;;;;  (21188 53208 6216 425000))
;;; Generated autoloads from misc-fns.el

(defvar mode-line-reminder-duration 10 "\
*Maximum number of seconds to display a reminder in the mode-line.")

(custom-autoload 'mode-line-reminder-duration "misc-fns" t)

;;;***

;;;### (autoloads nil nil ("misc-fns-pkg.el") (21188 53208 128873
;;;;;;  43000))

;;;***

(provide 'misc-fns-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; misc-fns-autoloads.el ends here
