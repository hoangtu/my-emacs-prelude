;;; finder+-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (finder-commentary) "finder+" "finder+.el" (21188
;;;;;;  53326 558218 310000))
;;; Generated autoloads from finder+.el

(autoload 'finder-commentary "finder+" "\
Display FILE's commentary section.
FILE should be in a form suitable for passing to `locate-library'.

\(fn FILE)" t nil)

;;;***

;;;### (autoloads nil nil ("finder+-pkg.el") (21188 53326 660992
;;;;;;  699000))

;;;***

(provide 'finder+-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; finder+-autoloads.el ends here
