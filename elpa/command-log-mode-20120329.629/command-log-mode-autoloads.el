;;; command-log-mode-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (command-log-mode) "command-log-mode" "command-log-mode.el"
;;;;;;  (21264 8872 173122 111000))
;;; Generated autoloads from command-log-mode.el

(autoload 'command-log-mode "command-log-mode" "\
Toggle keyboard command logging.

\(fn &optional ARG)" t nil)

;;;***

;;;### (autoloads nil nil ("command-log-mode-pkg.el") (21264 8872
;;;;;;  284836 622000))

;;;***

(provide 'command-log-mode-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; command-log-mode-autoloads.el ends here
