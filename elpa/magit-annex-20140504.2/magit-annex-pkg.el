(define-package "magit-annex" "20140504.2" "Use git annex within magit" '((cl-lib "0.3") (magit "1.2.0")) :url "https://github.com/kyleam/magit-annex" :keywords ("magit" "git-annex"))
