;;; world-time-mode-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (list-world-time world-time-list) "world-time-mode"
;;;;;;  "world-time-mode.el" (21239 3813 646843 390000))
;;; Generated autoloads from world-time-mode.el

(autoload 'world-time-list "world-time-mode" "\
Show `display-time-world-list' full day comparison.

\(fn)" t nil)

(autoload 'list-world-time "world-time-mode" "\
Show `display-time-world-list' full day comparison.

\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("world-time-mode-pkg.el") (21239 3813
;;;;;;  790692 380000))

;;;***

(provide 'world-time-mode-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; world-time-mode-autoloads.el ends here
