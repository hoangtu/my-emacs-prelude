;;; djvu-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (djvu-find-file) "djvu" "djvu.el" (21121 37414
;;;;;;  438655 912000))
;;; Generated autoloads from djvu.el

(autoload 'djvu-find-file "djvu" "\
Read and edit Djvu FILE on PAGE.
If VIEW is non-nil start external viewer.

\(fn FILE &optional PAGE VIEW)" t nil)

;;;***

;;;### (autoloads nil nil ("djvu-pkg.el") (21121 37414 554172 717000))

;;;***

(provide 'djvu-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; djvu-autoloads.el ends here
