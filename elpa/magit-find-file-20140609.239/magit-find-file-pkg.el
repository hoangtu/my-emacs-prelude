(define-package "magit-find-file" "20140609.239" "completing-read over all files in Git" '((magit "1.2.0")) :url "https://github.com/bradleywright/magit-find-file.el" :keywords ("git"))
