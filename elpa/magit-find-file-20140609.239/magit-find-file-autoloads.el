;;; magit-find-file-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (or (file-name-directory #$) (car load-path)))

;;;### (autoloads nil "magit-find-file" "magit-find-file.el" (21405
;;;;;;  8579 741031 371000))
;;; Generated autoloads from magit-find-file.el

(autoload 'magit-get-top-dir "magit")

(autoload 'magit-git-lines "magit")

(autoload 'magit-find-file-completing-read "magit-find-file" "\
Uses a completing read to open a file from git ls-files

\(fn)" t nil)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; magit-find-file-autoloads.el ends here
