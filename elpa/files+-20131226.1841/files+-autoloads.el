;;; files+-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (dired-mouse-describe-listed-directory dired-describe-listed-directory)
;;;;;;  "files+" "files+.el" (21188 53330 370218 370000))
;;; Generated autoloads from files+.el

(autoload 'dired-describe-listed-directory "files+" "\
In Dired, describe the current listed directory.

\(fn)" t nil)

(autoload 'dired-mouse-describe-listed-directory "files+" "\
Describe the current listed directory.

\(fn EVENT)" t nil)

;;;***

;;;### (autoloads nil nil ("files+-pkg.el") (21188 53330 449652 561000))

;;;***

(provide 'files+-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; files+-autoloads.el ends here
