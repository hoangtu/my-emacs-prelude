(define-package "mustache" "20131117.2207" "a mustache templating library in emacs lisp [github]"
  '((ht "0.9")
    (s "1.3.0")
    (dash "1.2.0")))
