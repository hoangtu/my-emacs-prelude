;ELC   
;;; Compiled by solidius4747@gmail.com on Fri Nov 29 03:15:09 2013
;;; from file /home/hoangtu/.emacs.d/elpa/look-dired-20130512.24/look-dired.el
;;; in Emacs version 24.3.1
;;; with all optimizations.

;;; This file uses dynamic docstrings, first added in Emacs 19.29.

;;; This file does not contain utf-8 non-ASCII characters,
;;; and so can be loaded in Emacs versions earlier than 23.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(require 'look-mode)
#@62 the target of `look-current-file' in `look-dired-do-rename'.
(defvar look-dired-rename-target nil (#$ . 564))
#@73 the associated dired-mode buffer, from which `look-at-files' is called.
(defvar look-dired-buffer nil (#$ . 680))
(byte-code "\301\302\303#\210\301\304\305#\210\301\306\307#\210\301\310\311#\210\301\312\313#\210\301\314\315#\210\316\317\320\321\322$\210\323\317\322\"\207" [look-minor-mode-map define-key [134217842] look-dired-do-rename [134217837] look-dired-mark-current-looked-file [134217805] look-dired-mark-looked-files [134217845] look-dired-unmark-current-looked-file [134217813] look-dired-unmark-looked-files [134217741] look-dired-run-associated-program ad-add-advice look-reset-variables (look-dired-reset-variables nil t (advice lambda nil "Reset `look-dired-rename-target' and `look-dired-buffer'." (setq look-dired-rename-target nil) (setq look-dired-buffer nil))) after nil ad-activate] 5)
#@152 Look at files in a directory.  Insert them into a temporary
buffer one at a time.  This function gets the file list and passes
it to look-at-next-file
(defalias 'look-at-files #[(look-wildcard &optional dired-buffer) "\306\307\n\"\203 \310\311!\204 \312\311\313\314#\210\n\315\230\203 \316\313\317C\313\211#\320\321\322\323!\320\324\315\325 ##$\313%&\326=\203O \327 \203I \330 \206R \331\n!\202R \331\n!\313'\211(\313)\211*\203\267 *@)\332)!\203 \333\334\215\203 $)PC\244\202\256 \335)!\203\256 \333\336\215\203\256 +\203\246 ')C\244\337$)P,\"\244'\202\256 ')C\244'*A\211*\204a \313)\210*'\313-\211*\203\343 *@-\f\340\320$\315-#!C\244*A\211*\204\310 \313-\f\210,\341.!\210\342 \207" [dired-buffer look-dired-buffer look-wildcard look-forward-file-list look-subdir-list look-reverse-file-list string-match "[Jj][Pp][Ee]?[Gg]" featurep eimp require nil t "" "*" "./" replace-regexp-in-string "~" getenv "HOME" "^Directory " pwd dired-mode look-dired-has-marked-file look-dired-get-marked-files file-expand-wildcards file-regular-p skip-this-one (byte-code "\304\211\203 \n@\305	\"\203 \306\307\304\"\210\nA\211\204 \304*\310\207" [look-skip-file-list regexp --dolist-tail-- lfl-item nil string-match throw skip-this-one t] 4) file-directory-p (byte-code "\304\211\203 \n@\305	\"\203 \306\307\304\"\210\nA\211\204 \304*\310\207" [look-skip-directory-list regexp --dolist-tail-- lfl-item nil string-match throw skip-this-one t] 4) list-subdirectories-recursively file-name-as-directory get-buffer-create look-at-next-file look-current-file look-pwd look-dired-rename-target major-mode fullpath-dir-list look-file-list lfl-item --dolist-tail-- look-recurse-dirlist look-skip-directory-list fullpath look-buffer] 8 (#$ . 1500) (byte-code "\301=\203 \302 \203 \303pD\207\304\305!C\207" [major-mode dired-mode look-dired-has-marked-file "" read-from-minibuffer "Enter filename (w/ wildcards): "] 2)])
#@100 Get all the marked files in current dired buffer.
The returned file names are relative file names.
(defalias 'look-dired-get-marked-files #[nil "\301 \302\303\")\207" [file-list dired-get-marked-files mapcar #[(file) "\302\303P\304	#\207" [look-pwd file replace-regexp-in-string "^" ""] 4]] 3 (#$ . 3476)])
#@63 Return `t' if there are marked files in current dired buffer.
(defalias 'look-dired-has-marked-file #[nil "\212eb\210\300\301 \302\303#\205 \304 )??\207" [re-search-forward dired-marker-regexp nil t point-marker] 5 (#$ . 3792)])
#@994 Rename current looked file, to location given by TARGET.
`look-current-file' will be removed from the *look* buffer
if the rename succeeds.
When TARGET is `nil' or prompt is non-nil, prompt for the location.
PREFIX and SUFFIX specify strings to be placed before and after the cursor in the prompt,
 (but after the target dir). PREFIX or SUFFIX may also be functions that take a single string 
argument and return a string. In this case they will be called with the argument set to the 
filename of the current looked file (without the directory part).
If PROMPT is nil the file will be moved to this directory while retaining the same filename, unless 
PREFIX and/or SUFFIX are non-nil in which case the filename will be changed to the concatenation of 
PREFIX and SUFFIX.
This command also renames any buffers that are visiting the files.
The default suggested for the target directory depends on the value
of `dired-dwim-target' (usually the directory in which the current file is located).
(defalias 'look-dired-do-rename #[(&optional target prompt prefix suffix) "\204 \306\202	 	\307\n!\203 \n\310!!\202 \n\307\f!\203& \f\310!!\202' \f\311\312\313\314\315\316	\315&\210\317!?\205G \315\320 +\207" [target prompt prefix look-current-file suffix post t functionp file-name-nondirectory look-dired-do-create-file move dired-rename-file "Move" nil "Rename" file-exists-p look-at-next-file pre dired-keep-marker-rename] 12 (#$ . 4029) nil])
#@1885 Create a new file for `look-current-file'.
Prompts user for target, which is a directory in which to create
  the new files.  Target may also be a plain file if only one marked
  file exists.  The way the default for the target directory is
  computed depends on the value of `dired-dwim-target-directory'.
OP-SYMBOL is the symbol for the operation.  Function `dired-mark-pop-up'
  will determine whether pop-ups are appropriate for this OP-SYMBOL.
FILE-CREATOR and OPERATION as in `dired-create-files'.
ARG as in `dired-get-marked-files'.
Optional arg MARKER-CHAR as in `dired-create-files'.
Optional arg OP1 is an alternate form for OPERATION if there is
  only one file.
Optional arg PROMPT determines whether prompts for the target location.
`nil' means not prompt and TARGET-FILE is the target location, non-nil
means prompt for the target location.
Optional arg HOW-TO determiness how to treat the target.
  If HOW-TO is nil, use `file-directory-p' to determine if the
   target is a directory.  If so, the marked file(s) are created
   inside that directory.  Otherwise, the target is a plain file;
   an error is raised unless there is exactly one marked file.
  If HOW-TO is t, target is always treated as a plain file.
  Otherwise, HOW-TO should be a function of one argument, TARGET.
   If its return value is nil, TARGET is regarded as a plain file.
   If it return value is a list, TARGET is a generalized
    directory (e.g. some sort of archive).  The first element of
    this list must be a function with at least four arguments:
      operation - as OPERATION above.
      rfn-list  - list of the relative names for the marked files.
      fn-list   - list of the absolute names for the marked files.
      target    - the name of the target itself.
      The rest of into-dir are optional arguments.
   For any other return value, TARGET is treated as a directory.
(defalias 'look-dired-do-create-file #[(op-symbol file-creator operation arg &optional marker-char op1 prompt target-file how-to prefix suffix) "\204 	\nC\306\307\":\205 A?\205 @\203/ \310!\203/ \310!\2022 \310\n!\203C Q\202a \205a \311\204R \203Z P\202^ \312@!\"\204r Q\202\220 \311\313\203| \202} 	\314PP !\f&!\"#\204\314 $\315>\205\301  \316=\205\301 \205\301 \311@!\227\311\"!\227\230\205\301 \312@!\312\"!\230??\205\334 \317\"!\202\334 #\320=\203\327 \321\202\334 #\"!\211%:\203\374 \322%@!\203\374 \323%@	\f\"%A&\202>\204\f%\204\f\324\325	\"#\210%\204\326\"!\"%\203(\311\312\n!\"\"&\202,\"&\327'	%\203:\330\202;\331(%.\207" [op1 operation look-current-file fn-list rfn-list dired-one-file mapcar dired-make-relative file-name-directory expand-file-name file-name-nondirectory look-dired-mark-read-file-name " %s to: " (ms-dos windows-nt cygwin) move file-directory-p t nil functionp apply error "Marked %s: target must be a directory: %s" directory-file-name dired-create-files #[(from) "\302\303!	\"\207" [from target expand-file-name file-name-nondirectory] 3] #[(from) "\207" [target] 1] target-file target-dir prefix suffix default prompt op-symbol arg target how-to system-type into-dir look-dired-rename-target file-creator marker-char] 10 (#$ . 5497)])
(defalias 'look-dired-mark-read-file-name #[(prompt dir op-symbol arg files &optional default initial) "\306\307	\310\311\n\312	\"\"\f\307&	\207" [op-symbol files prompt arg dir default dired-mark-pop-up nil read-file-name format dired-mark-prompt initial] 10])
#@216 Unmark all the files in `look-buffer' in the corresponding dired-mode buffer.
This is only meaningful when `look-buffer' has an associated dired-mode buffer,
i.e. `look-at-files' is called from a dired-mode buffer.
(defalias 'look-dired-unmark-looked-files #[nil "\205 \305	\nC#\306\307\f\"\210)\310\311!\207" [look-dired-buffer look-forward-file-list look-current-file look-reverse-file-list file-list append mapc look-dired-unmark-file message "Unmarked all looked at files in dired buffer"] 4 (#$ . 9031) nil])
#@214 Mark all the files in `look-buffer' in the corresponding dired-mode buffer.
This is only meaningful when `look-buffer' has an associated dired-mode buffer,
i.e. `look-at-files' is called from a dired-mode buffer.
(defalias 'look-dired-mark-looked-files #[nil "\205 \305	\nC#\306\307\f\"\210)\310\311!\207" [look-dired-buffer look-forward-file-list look-current-file look-reverse-file-list file-list append mapc look-dired-mark-file message "Marked all looked at files in dired buffer"] 4 (#$ . 9556) nil])
#@270 Mark `look-current-file' in the corresponding dired-mode buffer.
When SHOW-NEXT-FILE is non-nil, the next file will be looked in `look-buffer'.
Similar to `look-dired-unmark-looked-files', this function only work when
`look-buffer' has an associated dired-mode buffer.
(defalias 'look-dired-mark-current-looked-file #[(&optional show-next-file) "\205 \303	!\210\304\305\306	!\307Q!\210\n\205 \310 \207" [look-dired-buffer look-current-file show-next-file look-dired-mark-file message "Marked " file-name-nondirectory " in dired buffer" look-at-next-file] 4 (#$ . 10073) nil])
#@272 Unmark `look-current-file' in the corresponding dired-mode buffer.
When SHOW-NEXT-FILE is non-nil, the next file will be looked in `look-buffer'.
Similar to `look-dired-unmark-looked-files', this function only work when
`look-buffer' has an associated dired-mode buffer.
(defalias 'look-dired-unmark-current-looked-file #[(&optional show-next-file) "\205 \303	!\210\304\305\306	!\307Q!\210\n\205 \310 \207" [look-dired-buffer look-current-file show-next-file look-dired-unmark-file message "Unmarked " file-name-nondirectory " in dired buffer" look-at-next-file] 4 (#$ . 10659) nil])
#@42 `dired-mark' FILE in `look-dired-buffer'
(defalias 'look-dired-mark-file #[(file) "\204\n \301\302\300C\"\210\303!\205 \212q\210eb\210\304\305\215)\207" [look-dired-buffer signal cl-assertion-failed buffer-live-p --cl-block-nil-- (byte-code "m?\205. \303!\204( l\204( \304\305\306\"\211\205 	\n\230)\203( \307\310!\210\311\312\305\"\210\310y\210\202  \207" [dired-re-dot fn file looking-at dired-get-filename nil t dired-mark 1 throw --cl-block-nil--] 4)] 3 (#$ . 11252)])
#@83 Run program associated with currently looked at file.
Requires run-assoc library.
(defalias 'look-dired-run-associated-program #[nil "\301\302!\210\303!\207" [look-current-file require run-assoc run-associated-program] 2 (#$ . 11738) nil])
#@44 `dired-unmark' FILE in `look-dired-buffer'
(defalias 'look-dired-unmark-file #[(file) "\204\n \301\302\300C\"\210\303!\205 \212q\210eb\210\304\305\215)\207" [look-dired-buffer signal cl-assertion-failed buffer-live-p --cl-block-nil-- (byte-code "m?\205. \303!\204( l\204( \304\305\306\"\211\205 	\n\230)\203( \307\310!\210\311\312\305\"\210\310y\210\202  \207" [dired-re-dot fn file looking-at dired-get-filename nil t dired-unmark 1 throw --cl-block-nil--] 4)] 3 (#$ . 11985)])
(provide 'look-dired)
