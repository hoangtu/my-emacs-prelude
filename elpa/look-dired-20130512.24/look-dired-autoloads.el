;;; look-dired-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (look-dired-run-associated-program look-dired-unmark-current-looked-file
;;;;;;  look-dired-mark-current-looked-file look-dired-mark-looked-files
;;;;;;  look-dired-unmark-looked-files look-dired-do-rename look-at-files)
;;;;;;  "look-dired" "look-dired.el" (21143 42060 933685 706000))
;;; Generated autoloads from look-dired.el

(autoload 'look-at-files "look-dired" "\
Look at files in a directory.  Insert them into a temporary
buffer one at a time.  This function gets the file list and passes
it to look-at-next-file

\(fn LOOK-WILDCARD &optional DIRED-BUFFER)" t nil)

(autoload 'look-dired-do-rename "look-dired" "\
Rename current looked file, to location given by TARGET.
`look-current-file' will be removed from the *look* buffer
if the rename succeeds.
When TARGET is `nil' or prompt is non-nil, prompt for the location.
PREFIX and SUFFIX specify strings to be placed before and after the cursor in the prompt,
 (but after the target dir). PREFIX or SUFFIX may also be functions that take a single string 
argument and return a string. In this case they will be called with the argument set to the 
filename of the current looked file (without the directory part).
If PROMPT is nil the file will be moved to this directory while retaining the same filename, unless 
PREFIX and/or SUFFIX are non-nil in which case the filename will be changed to the concatenation of 
PREFIX and SUFFIX.
This command also renames any buffers that are visiting the files.
The default suggested for the target directory depends on the value
of `dired-dwim-target' (usually the directory in which the current file is located).

\(fn &optional TARGET PROMPT PREFIX SUFFIX)" t nil)

(autoload 'look-dired-unmark-looked-files "look-dired" "\
Unmark all the files in `look-buffer' in the corresponding dired-mode buffer.
This is only meaningful when `look-buffer' has an associated dired-mode buffer,
i.e. `look-at-files' is called from a dired-mode buffer.

\(fn)" t nil)

(autoload 'look-dired-mark-looked-files "look-dired" "\
Mark all the files in `look-buffer' in the corresponding dired-mode buffer.
This is only meaningful when `look-buffer' has an associated dired-mode buffer,
i.e. `look-at-files' is called from a dired-mode buffer.

\(fn)" t nil)

(autoload 'look-dired-mark-current-looked-file "look-dired" "\
Mark `look-current-file' in the corresponding dired-mode buffer.
When SHOW-NEXT-FILE is non-nil, the next file will be looked in `look-buffer'.
Similar to `look-dired-unmark-looked-files', this function only work when
`look-buffer' has an associated dired-mode buffer.

\(fn &optional SHOW-NEXT-FILE)" t nil)

(autoload 'look-dired-unmark-current-looked-file "look-dired" "\
Unmark `look-current-file' in the corresponding dired-mode buffer.
When SHOW-NEXT-FILE is non-nil, the next file will be looked in `look-buffer'.
Similar to `look-dired-unmark-looked-files', this function only work when
`look-buffer' has an associated dired-mode buffer.

\(fn &optional SHOW-NEXT-FILE)" t nil)

(autoload 'look-dired-run-associated-program "look-dired" "\
Run program associated with currently looked at file.
Requires run-assoc library.

\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("look-dired-pkg.el") (21143 42061 41956
;;;;;;  195000))

;;;***

(provide 'look-dired-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; look-dired-autoloads.el ends here
