(define-package "persp-projectile" "20140603.48" "Perspective integration with Projectile" '((perspective "1.9") (projectile "0.11.0") (cl-lib "0.3")) :keywords ("project" "convenience"))
