;;; framesize-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (frame-font-keychord-init frame-font-smaller frame-font-bigger)
;;;;;;  "framesize" "framesize.el" (21188 53323 506218 261000))
;;; Generated autoloads from framesize.el

(autoload 'frame-font-bigger "framesize" "\


\(fn)" t nil)

(autoload 'frame-font-smaller "framesize" "\


\(fn)" t nil)

(autoload 'frame-font-keychord-init "framesize" "\


\(fn)" nil nil)

;;;***

;;;### (autoloads nil nil ("framesize-pkg.el") (21188 53323 579452
;;;;;;  635000))

;;;***

(provide 'framesize-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; framesize-autoloads.el ends here
