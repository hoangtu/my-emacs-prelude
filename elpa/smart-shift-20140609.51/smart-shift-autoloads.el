;;; smart-shift-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (or (file-name-directory #$) (car load-path)))

;;;### (autoloads nil "smart-shift" "smart-shift.el" (21405 8495
;;;;;;  29034 25000))
;;; Generated autoloads from smart-shift.el

(defvar smart-shift-mode-alist '((lisp-mode . lisp-body-indent) (emacs-lisp-mode . lisp-body-indent) (c-mode . c-basic-offset) (c++-mode . c-basic-offset) (objc-mode . c-basic-offset) (java-mode . c-basic-offset) (idl-mode . c-basic-offset) (pike-mode . c-basic-offset) (awk-mode . c-basic-offset) (ruby-mode . ruby-indent-level) (python-mode . python-indent-offset) (swift-mode . swift-indent-offset) (js-mode . js-indent-level) (js2-mode . js2-basic-offset) (coffee-mode . coffee-tab-width) (css-mode . css-indent-offset) (slim-mode . slim-indent-offset) (html-mode . sgml-basic-offset) (web-mode lambda nil (cond ((string= web-mode-content-type "css") web-mode-css-indent-offset) ((member web-mode-content-type '("javascript" "json" "jsx" "php")) web-mode-code-indent-offset) (t web-mode-markup-indent-offset))) (sh-mode . sh-basic-offset) (yaml-mode . yaml-indent-offset) (text-mode . tab-width) (fundamental-mode . tab-width)) "\
Alist which maps major modes to its indentation-level.")

(custom-autoload 'smart-shift-mode-alist "smart-shift" t)

(defvar smart-shift-indentation-level nil "\
Variable used to specify the indentation-level for the current buffer.")

(autoload 'smart-shift-infer-indentation-level "smart-shift" "\
Infer indentation-level of current major mode.

\(fn)" nil nil)

(autoload 'smart-shift-right "smart-shift" "\
Shift the line or region to the ARG times to the right.

\(fn &optional ARG)" t nil)

(autoload 'smart-shift-left "smart-shift" "\
Shift the line or region to the ARG times to the left.

\(fn &optional ARG)" t nil)

(autoload 'smart-shift-mode "smart-shift" "\
Shift line/region to left/right.

\(fn &optional ARG)" t nil)

(autoload 'smart-shift-mode-on "smart-shift" "\
Turn on smart-shift mode.

\(fn)" nil nil)

(autoload 'smart-shift-mode-off "smart-shift" "\
Turn off smart-shift mode.

\(fn)" nil nil)

(defvar global-smart-shift-mode nil "\
Non-nil if Global-Smart-Shift mode is enabled.
See the command `global-smart-shift-mode' for a description of this minor mode.
Setting this variable directly does not take effect;
either customize it (see the info node `Easy Customization')
or call the function `global-smart-shift-mode'.")

(custom-autoload 'global-smart-shift-mode "smart-shift" nil)

(autoload 'global-smart-shift-mode "smart-shift" "\
Toggle Smart-Shift mode in all buffers.
With prefix ARG, enable Global-Smart-Shift mode if ARG is positive;
otherwise, disable it.  If called from Lisp, enable the mode if
ARG is omitted or nil.

Smart-Shift mode is enabled in all buffers where
`smart-shift-mode-on' would do it.
See `smart-shift-mode' for more information on Smart-Shift mode.

\(fn &optional ARG)" t nil)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; smart-shift-autoloads.el ends here
