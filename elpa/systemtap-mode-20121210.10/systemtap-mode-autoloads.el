;;; systemtap-mode-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (systemtap-mode) "systemtap-mode" "systemtap-mode.el"
;;;;;;  (21145 22488 86737 570000))
;;; Generated autoloads from systemtap-mode.el

(add-to-list 'auto-mode-alist '("\\.stp\\'" . systemtap-mode))

(autoload 'systemtap-mode "systemtap-mode" "\
Major mode for editing SystemTap scripts.

Key bindings:
\\{systemtap-mode-map}

\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("systemtap-mode-pkg.el") (21145 22488
;;;;;;  202866 606000))

;;;***

(provide 'systemtap-mode-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; systemtap-mode-autoloads.el ends here
