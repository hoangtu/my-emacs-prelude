;;; esh-buf-stack-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (eshell-push-command eshell-pop-stack setup-eshell-buf-stack)
;;;;;;  "esh-buf-stack" "esh-buf-stack.el" (21201 22159 755395 661000))
;;; Generated autoloads from esh-buf-stack.el

(autoload 'setup-eshell-buf-stack "esh-buf-stack" "\
Setup the buffer stack for Eshell.

\(fn)" t nil)

(autoload 'eshell-pop-stack "esh-buf-stack" "\
Pop a command from the buffer stack.

\(fn)" t nil)

(autoload 'eshell-push-command "esh-buf-stack" "\
Push CMD to the buffer stack.

\(fn CMD)" t nil)

;;;***

;;;### (autoloads nil nil ("esh-buf-stack-pkg.el") (21201 22159 855279
;;;;;;  371000))

;;;***

(provide 'esh-buf-stack-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; esh-buf-stack-autoloads.el ends here
