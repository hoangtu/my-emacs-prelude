;;; xmlgen-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (xmlgen) "xmlgen" "xmlgen.el" (21161 23580 899120
;;;;;;  48000))
;;; Generated autoloads from xmlgen.el

(autoload 'xmlgen "xmlgen" "\
Convert a sexp to xml:
  '(p :class \"big\")) => \"<p class=\\\"big\\\" />\"

\(fn FORM &optional IN-ELM LEVEL)" nil nil)

;;;***

;;;### (autoloads nil nil ("xmlgen-pkg.el") (21161 23581 114040 629000))

;;;***

(provide 'xmlgen-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; xmlgen-autoloads.el ends here
