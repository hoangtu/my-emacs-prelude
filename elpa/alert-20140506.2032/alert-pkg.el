(define-package "alert" "20140506.2032" "Growl-style notification system for Emacs" '((gntp "0.1")) :url "https://github.com/jwiegley/alert" :keywords ("notification" "emacs" "message"))
