;; skeleton .yas-setup.el file for tcl-mode
;;
(load (concat *emacs-dir* "/snippets/tcl-mode/.yasmate-menu.el"))
(yas-define-menu 'tcl-mode
                 '(;; Ignoring Run
                   (yas-ignore-item "59700A78-7CB7-11D9-875B-000A95E13C98")
                   
                   ;; Ignoring Documentation for Word / Selection (man)
                   (yas-ignore-item "56F8C50E-B263-430C-9301-A34EDF05E9F5")
                   
                   ;; for...
                   (yas-item "35FD4583-34CA-446A-958B-D6446220B2BF")
                   ;; foreach...
                   (yas-item "686ACE8E-AF2D-4429-BD3D-DB2272DE22FE")
                   ;; if...
                   (yas-item "3BF57469-548C-4745-BB68-127C29CE70A1")
                   ;; proc...
                   (yas-item "16FB1812-6644-416D-8544-03F7E9C7C45E")
                   ;; switch...
                   (yas-item "9301C846-1EED-4749-AB9D-2728EA661E97")
                   ;; while...
                   (yas-item "6F958C1C-2A05-4794-A551-4C3ABAAEE817")
                   (yas-separator)
                   ;; #!/usr/bin/env tcl
                   (yas-item "A2C95E2E-6D2F-4718-9708-2C99FF5FDB11"))
                    '("56F8C50E-B263-430C-9301-A34EDF05E9F5"
                       "59700A78-7CB7-11D9-875B-000A95E13C98"))
