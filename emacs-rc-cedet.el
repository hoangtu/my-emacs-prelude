;;; emacs-rc-cedet.el ---

;;(add-to-list 'load-path "~/projects/cedet-bzr/contrib/")
;; (add-to-list 'Info-directory-list "~/projects/cedet-bzr/doc/info")
(load-file (concat *emacs-dir* "/cedet/cedet-devel-load.el"))
(add-to-list 'load-path (concat *emacs-dir* "/cedet/contrib"))
(load-file (concat *emacs-dir* "/cedet/contrib/cedet-contrib-load.el"))
(add-to-list 'semantic-default-submodes 'global-semanticdb-minor-mode)
(add-to-list 'semantic-default-submodes 'global-semantic-mru-bookmark-mode)
(add-to-list 'semantic-default-submodes 'global-semanticdb-minor-mode)
(add-to-list 'semantic-default-submodes 'global-semantic-idle-scheduler-mode)
;; (add-to-list 'semantic-default-submodes 'global-semantic-idle-summary-mode)
;;(add-to-list 'semantic-default-submodes 'global-semantic-stickyfunc-mode)
;; (add-to-list 'semantic-default-submodes 'global-cedet-m3-minor-mode)
;; (add-to-list 'semantic-default-submodes 'global-semantic-highlight-func-mode)
;;(add-to-list 'semantic-default-submodes 'global-semantic-show-unmatched-syntax-mode)
;;(add-to-list 'semantic-default-submodes 'global-semantic-highlight-edits-mode)
;;(add-to-list 'semantic-default-submodes 'global-semantic-show-parser-state-mode)

;; Activate semantic
(semantic-mode 1)

(require 'semantic/bovine/c)
(require 'semantic/bovine/gcc)
(require 'semantic/bovine/clang)

(require 'cedet-files)

(semantic-gcc-setup)
;; turn on cscope semanticdb backend
;; (require 'semanticdb-cscope)
;; (semanticdb-enable-cscope-databases)
;; (when (cedet-ectag-version-check)
;;   (semantic-load-enable-primary-exuberent-ctags-support))

;; loading contrib...
(require 'eassist)
(add-hook 'c++-mode-hook (lambda () (semantic-idle-summary-mode 1)))
;; customisation of modes
(defun alexott/cedet-hook ()
  ;; (local-set-key [(control return)] 'semantic-ia-complete-symbol-menu)
  (local-set-key "\C-c\C-?" 'semantic-ia-complete-symbol)
  ;; (local-set-key "\C-c>" 'semantic-complete-analyze-inline)
  (local-set-key "\C-c\C-v" 'semantic-decoration-include-visit)
  (local-set-key "\C-c\C-j" 'my-semantic-ia-fast-jump)
  (local-set-key "\C-c\C-d" 'semantic-ia-show-doc)
  (local-set-key "\C-c\C-s" 'semantic-ia-show-summary)
  (local-set-key "\C-c\C-y" 'semantic-analyze-proto-impl-toggle)
  ;; (local-set-key (kbd "C-c C-<up>") 'semantic-tag-folding-fold-block)
  ;; (local-set-key (kbd "C-c C-<down>") 'semantic-tag-folding-show-block)
  ;; (local-set-key (kbd "M-i") 'windmove-up)
  ;; (local-set-key (kbd "M-j") 'windmove-left)
  ;; (local-set-key (kbd "M-l") 'windmove-right)
  ;; (local-set-key (kbd "M-k") 'windmove-down)
  ;; (local-set-key (kbd "C-<return>") 'c-indent-new-comment-line)
  (add-to-list 'ac-sources 'ac-source-semantic)
  )
;; (add-hook 'semantic-init-hooks 'alexott/cedet-hook)
(add-hook 'c-mode-common-hook 'alexott/cedet-hook)
(add-hook 'c-mode-hook 'alexott/cedet-hook)
(add-hook 'c++-mode-hook 'alexott/cedet-hook)
(add-hook 'lisp-mode-hook 'alexott/cedet-hook)
(add-hook 'scheme-mode-hook 'alexott/cedet-hook)
(add-hook 'emacs-lisp-mode-hook 'alexott/cedet-hook)
(add-hook 'erlang-mode-hook 'alexott/cedet-hook)

(defun alexott/c-mode-cedet-hook ()
  ;; (local-set-key "." 'semantic-complete-self-insert)
  ;; (local-set-key ">" 'semantic-complete-self-insert)
  ;; (local-set-key "\C-ct" 'eassist-switch-h-cpp)
  (local-set-key "\C-ct" 'eassist-switch-h-cpp)
  (local-set-key "\C-c\C-r" 'semantic-symref)

  ;; (add-to-list 'ac-sources 'ac-source-gtags)
  ;; (add-to-list 'ac-sources 'ac-source-etags)
  ;; (add-to-list 'ac-sources 'ac-source-c-headers)
  )
(add-hook 'c-mode-common-hook 'alexott/c-mode-cedet-hook)
(add-hook 'c-mode-hook 'alexott/c-mode-cedet-hook)
(add-hook 'c++-mode-hook 'alexott/c-mode-cedet-hook)

(setq cedet-global-command "global")
(setq-default semantic-symref-tool 'global)
(when (cedet-gnu-global-version-check t)
  (semanticdb-enable-gnu-global-databases 'c-mode t)
  (semanticdb-enable-gnu-global-databases 'c++-mode t))

(semantic-add-system-include "~/linux/kernel" 'c-mode)
(semantic-add-system-include "~/linux/include" 'c-mode)
(semantic-add-system-include "~/linux/kernel" 'c++-mode)
(semantic-add-system-include "~/linux/include" 'c++-mode)

;; SRecode
(global-srecode-minor-mode 1)

;; EDE
(global-ede-mode 1)
(add-to-list 'dired-mode-hook 'ede-dired-minor-mode)
(add-to-list 'c-mode-hook 'ede-minor-mode)
(add-to-list 'c++-mode-hook 'ede-minor-mode)
(ede-enable-generic-projects)
(setq ede-locate-setup-options
      '(ede-locate-global
        ede-locate-base))

;; helper for boost setup...
(defun c++-setup-boost (boost-root)
  (when (file-accessible-directory-p boost-root)
    (let ((cfiles (cedet-files-list-recursively boost-root "\\(config\\|user\\)\\.hpp")))
      (dolist (file cfiles)
        (add-to-list 'semantic-lex-c-preprocessor-symbol-file file)))))


;; my functions for EDE
(defun alexott/ede-get-local-var (fname var)
  "fetch given variable var from :local-variables of project of file fname"
  (let* ((current-dir (file-name-directory fname))
         (prj (ede-current-project current-dir)))
    (when prj
      (let* ((ov (oref prj local-variables))
             (lst (assoc var ov)))
        (when lst
          (cdr lst))))))

(defun my-semantic-ia-fast-jump ()
  (interactive)
  (condition-case nil
      (progn
        (semantic-ia-fast-jump (point)))
    (error
     (progn
       (let ((p (point)))
         (push-mark-command (point) t)
         (deactivate-mark)
         (ignore-errors (ggtags-find-definition (ggtags-tag-at-point))))))))


;; setup compile package
(require 'compile)
(setq compilation-disable-input nil)
(setq compilation-scroll-output t)
(setq mode-compile-always-save-buffer-p t)

(defun alexott/compile ()
  "Saves all unsaved buffers, and runs 'compile'."
  (interactive)
  (save-some-buffers t)
  (let* ((fname (or (buffer-file-name (current-buffer)) default-directory))
         (current-dir (file-name-directory fname))
         (prj (ede-current-project current-dir)))
    (if prj
        (project-compile-project prj)
      (compile compile-command))))
;; (global-set-key [f9] 'alexott/compile)

;;
(defun alexott/gen-std-compile-string ()
  "Generates compile string for compiling CMake project in debug mode"
  (let* ((current-dir (file-name-directory
                       (or (buffer-file-name (current-buffer)) default-directory)))
         (prj (ede-current-project current-dir))
         (root-dir (ede-project-root-directory prj)))
    (concat "cd " root-dir "; make -j2")))

;;
(defun alexott/gen-cmake-debug-compile-string ()
  "Generates compile string for compiling CMake project in debug mode"
  (let* ((current-dir (file-name-directory
                       (or (buffer-file-name (current-buffer)) default-directory)))
         (prj (ede-current-project current-dir))
         (root-dir (ede-project-root-directory prj))
         (subdir "")
         )
    (when (string-match root-dir current-dir)
      (setf subdir (substring current-dir (match-end 0))))
    (concat "cd " root-dir "Debug/" "; make -j3")))

;; (semanticdb-enable-cscope-databases)
;;; Projects

;; cpp-tests project definition
(when (file-exists-p "~/projects/lang-exp/cpp/CMakeLists.txt")
  (setq cpp-tests-project
        (ede-cpp-root-project "cpp-tests"
                              :file "~/projects/lang-exp/cpp/CMakeLists.txt"
                              :system-include-path '("/home/ott/exp/include"
                                                     boost-base-directory)
                              :compile-command "cd Debug && make -j2"
                              )))

(when (file-exists-p "~/projects/squid-gsb/README")
  (setq squid-gsb-project
        (ede-cpp-root-project "squid-gsb"
                              :file "~/projects/squid-gsb/README"
                              :system-include-path '("/home/ott/exp/include"
                                                     boost-base-directory)
                              :compile-command "cd Debug && make -j2"
                              )))

;; Setup JAVA....
(require 'semantic/db-javap)

;; example of java-root project

;; (ede-ant-project "Lucene"
;;                  :file "~/work/lucene-solr/lucene-4.0.0/build.xml"
;;                  :srcroot '("core/src")
;;                  :classpath (cedet-files-list-recursively "~/work/lucene-solr/lucene-4.0.0/" ".*\.jar$")
