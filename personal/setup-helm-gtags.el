;;; Enable helm-gtags-mode
(add-hook 'c-mode-hook 'helm-gtags-mode)
(add-hook 'c++-mode-hook 'helm-gtags-mode)
(add-hook 'asm-mode-hook 'helm-gtags-mode)

;; customize
(custom-set-variables
 '(helm-gtags-path-style 'relative)
 '(helm-gtags-ignore-case t)
 '(helm-gtags-auto-update t))

;; key bindings
(eval-after-load "helm-gtags"
  '(progn
     (define-key helm-gtags-mode-map (kbd "M-t") 'helm-gtags-select)
     (define-key helm-gtags-mode-map (kbd "M-r") 'ggtags-find-reference)
     ;; (define-key helm-gtags-mode-map (kbd "M-s") 'gg-symbol)
     ;; (define-key helm-gtags-mode-map (kbd "M-g f") 'helm-gtags-parse-file)
     (define-key helm-gtags-mode-map (kbd "C-c <") 'helm-gtags-previous-history)
     (define-key helm-gtags-mode-map (kbd "C-c >") 'helm-gtags-next-history)
     ))

;; (global-set-key (kbd "M-g t") 'helm-gtags-select)
;; (global-set-key (kbd "M-g r") 'helm-gtags-find-rtag)
;; (global-set-key (kbd "M-g s") 'helm-gtags-find-symbol)
;; (global-set-key (kbd "M-g f") 'helm-gtags-parse-file)
;; (global-set-key (kbd "C-c <") 'helm-gtags-previous-history)
;; (global-set-key (kbd "C-c >") 'helm-gtags-next-history)

(provide 'setup-helm-gtags)
