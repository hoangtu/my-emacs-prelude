(setq eww-search-prefix "https://startpage.com/do/m/mobilesearch?query=")

(defun my-eww-browse-dwim ()
  "`eww' browse \"do what I mean\".
 Browse the url at point if there is one. Otherwise use the last
 kill-ring item and provide that to `eww'. If it is an url `eww'
 will browse it, if not `eww' will search for it using a search
 engine."
  (interactive)
  (let ((arg (or
              (url-get-url-at-point)
              (current-kill 0 t))))
    (eww arg)))

(provide 'setup-eww)
