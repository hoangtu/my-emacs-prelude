;; (defun fullpath-relative-to-current-file (file-relative-path)
;;   "Returns the full path of FILE-RELATIVE-PATH, relative to file location where this function is called.

;; Example: If you have this line
;;  (fullpath-relative-to-current-file \"../xyz.el\")
;; in the file at
;;  /home/mary/emacs/emacs_lib.el
;; then the return value is
;;  /home/mary/xyz.el
;; Regardless how or where emacs_lib.el is called.

;; This function solves 2 problems.

;;  ① If you have file A, that calls the `load' on a file at B, and
;; B calls “load” on file C using a relative path, then Emacs will
;; complain about unable to find C. Because, emacs does not switch
;; current directory with “load”.

;;  To solve this problem, when your code only knows the relative
;; path of another file C, you can use the variable `load-file-name'
;; to get the current file's full path, then use that with the
;; relative path to get a full path of the file you are interested.

;;  ② To know the current file's full path, emacs has 2 ways:
;; `load-file-name' and `buffer-file-name'. If the file is loaded
;; by “load”, then load-file-name works but buffer-file-name
;; doesn't. If the file is called by `eval-buffer', then
;; load-file-name is nil. You want to be able to get the current
;; file's full path regardless the file is run by “load” or
;; interactively by “eval-buffer”."
;;   (concat (file-name-directory (or load-file-name buffer-file-name)) file-relative-path)
;;   )

;; ,----
;; |
;; |
;; | PROGRAMMING SPECIFIC MODES AND CONFIGURATIONS
;; |
;; |
;; `----

;;auto-indent when enter
(dolist (command '(yank yank-pop))
  (eval `(defadvice ,command (after indent-region activate)
           (and (not current-prefix-arg)
                (member major-mode '(emacs-lisp-mode lisp-mode
                                                     clojure-mode    scheme-mode
                                                     haskell-mode    ruby-mode
                                                     rspec-mode      python-mode
                                                     c-mode          c++-mode
                                                     objc-mode       latex-mode
                                                     plain-tex-mode))
                (let ((mark-even-if-inactive transient-mark-mode))
                  (indent-region (region-beginning) (region-end) nil))))))
;; (define-key global-map (kbd "RET") 'newline-and-indent)

;;gas-mode
;; (load (fullpath-relative-to-current-file "gas-mode"))
;; (add-to-list 'load-path "~/.emacs.d/myplan")

;;asmMODE
;; (autoload 'asm86-mode "~/.emacs.d/asm86-mode")
;; (setq auto-mode-alist
;;       (append '(("\\.asm\\'" . asm86-mode) ("\\.inc\\'" . asm86-mode)) auto-mode-alist))
;; (add-hook 'asm86-mode-hook 'turn-on-font-lock)
;; (add-hook 'asm86-mode-hook
;;           '(lambda ()
;;              (define-key asm86-mode-map "\r" 'reindent-then-newline-and-indent)))

;;flex-mode
;; (load (fullpath-relative-to-current-file "flex-mode"))
(setq auto-mode-alist (cons '("\\.flex$" . flex-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.y$" . c++-mode) auto-mode-alist))

;;slime
(setq inferior-lisp-program "/usr/bin/sbcl") ; your Lisp system

(eval-after-load "slime"
  '(progn
     (setq slime-lisp-implementations
           '((sbcl ("/usr/bin/sbcl"))
             (ecl ("/usr/bin/ecl"))
             (clisp ("/usr/bin/clisp"))))
     (slime-setup '(
                    slime-asdf
                    slime-autodoc
                    slime-editing-commands
                    slime-fancy
                    slime-fancy-inspector
                    slime-fontifying-fu
                    ;; slime-fuzzy
                    slime-indentation
                                        ;slime-mdot-fu
                    slime-package-fu
                    slime-references
                    ;; slime-repl
                    slime-sbcl-exts
                    slime-scratch
                    slime-xref-browser
                    ))
     ;; (slime-setup '(slime-fancy))
     (slime-autodoc-mode)
     (setq slime-complete-symbol*-fancy t)
     (setq common-lisp-hyperspec-root (concat "file://" (getenv "HOME") "/.emacs.d/HyperSpec/"))
     (setq slime-complete-symbol-function 'slime-fuzzy-complete-symbol)))

(require 'slime)

;; emacs-lisp

(defun clone-buffer-and-narrow-to-function ()
  (interactive)
  (clone-indirect-buffer-other-window (buffer-name) 'pop-to-buffer)
  (elisp-slime-nav-find-elisp-thing-at-point (symbol-name (symbol-at-point))) ; works not only in emacs-lisp, but C++, Python, ...
  (narrow-to-defun)
  (pop-mark)
  )

(define-key global-map (kbd "C-x 4 n") 'clone-buffer-and-narrow-to-function)
;; ;; ,----
;; ;; |
;; ;; | C/C++
;; ;; |
;; ;; `----

;;set c++ style
(add-hook 'c++-mode-hook
          '(lambda ( )
             (c-set-style "Stroustrup")
             (c-toggle-auto-state)))

;;cc-mode linux style
(setq
 c-default-style "k&r"
 c-basic-offset 4
 c-hungry-delete-key t                  ;delete more than one space
 )

;; (defun my-ac-cc-mode-setup ()
;;   (setq ac-sources (append '(ac-source-clang ac-source-yasnippet) ac-sources)))
;; (add-hook 'c-mode-common-hook 'my-ac-cc-mode-setup)

;; (setq-default c-basic-offset 4
;;               tab-width 4
;;               indent-tabs-mode t)

;; (setq tab-always-indent 'complete) ;; already exist in Prelude
(add-to-list 'completion-styles 'initials t)

(setq auto-mode-alist (cons '("\\.cxx$" . c++-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.hpp$" . c++-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.lzz$" . c++-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.h$" . c++-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.tex$" . latex-mode) auto-mode-alist))

(define-key c-mode-base-map (kbd "C-c C-a") 'disaster)
;; (define-key c-mode-base-map (kbd "<tab>") 'ac-complete-with-helm)

;; (require 'google-c-style)
;; (add-hook 'c-mode-common-hook 'google-set-c-style)

(require 'function-args)
(fa-config-default)
(add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode))
(set-default 'semantic-case-fold t)

(add-hook 'c-mode-common-hook
          (lambda ()
            (when (derived-mode-p 'c-mode 'c++-mode 'java-mode)
              (ggtags-mode 1))))

(add-hook 'c-mode-hook (lambda () (setq-local company-idle-delay nil)))
(add-hook 'c++-mode-hook (lambda () (setq-local company-idle-delay nil)))

(setq gdb-many-windows t)
(setq gdb-show-main t)
(setq gud-chdir-before-run nil)
(setq gud-tooltip-mode t)

(defun c++-type-at (point)
  "Use semantic to determine the fully namespace-qualified type of the symbol at POINT."
  (interactive "d")
  (let* ((ctxt (semantic-analyze-current-context point))
         (pf (reverse (oref ctxt prefix)))
         (lastname (pop pf))
         (tag (if (semantic-tag-p lastname) lastname (car pf)))
         (names (append
                 (when (semantic-tag-p tag)
                   (save-excursion
                     (when (semantic-tag-with-position-p tag)
                       (set-buffer (semantic-tag-buffer tag))
                       (semantic-go-to-tag tag)
                       (mapcar 'semantic-tag-name (semantic-analyze-scope-nested-tags (point) nil)))))
                 (list (if (semantic-tag-p lastname) (semantic-tag-name lastname) lastname)))))
    (concat (mapconcat 'concat names "::"))))

(defvar c++-doco-sources
  '(("std::" .
     "http://en.cppreference.com/mwiki/index.php?title=Special:Search&search=%s")
    ("boost::" . "http://google.com/search?q=site:boost.org%%20%s")
    ))

(defun semantic-browse-c++-doc (point)
  "Browse the documentation for the C++ symbol at POINT."
  (interactive "d")
  (let* ((cpptype (c++-type-at point))
         (ref (when (stringp cpptype)
                (car (cl-member-if (lambda (S) (string-prefix-p (car S) cpptype))
                                   c++-doco-sources)))))
    (if ref
        (eww (format (cdr ref) cpptype))
      (progn
        (eww "https://startpage.com/do/m/mobilesearch?query=%s")
        ))))

;; ;; ,----
;; ;; |
;; ;; | Erlang-mode
;; ;; |
;; ;; `----
;; (setq load-path (cons  "/usr/local/lib/erlang/lib/tools-2.6.12/emacs/"
;;                        load-path))
;; (setq erlang-root-dir "/usr/local/lib/erlang")
;; (setq exec-path (cons "/usr/local/lib/erlang/bin" exec-path))
;; (require 'erlang-start)
;; ;;flymake-erlang
;; (require 'erlang-flymake)
;; (require 'flymake)
;; (defun flymake-erlang-init ()
;;   (let* ((temp-file (flymake-init-create-temp-buffer-copy
;;                      'flymake-create-temp-inplace))
;;          (local-file (file-relative-name temp-file
;;                                          (file-name-directory buffer-file-name))))
;;     (list (fullpath-relative-to-current-file "check_erlang.erl") (list local-file))))

;; (add-to-list 'flymake-allowed-file-name-masks '("\\.erl\\'" flymake-erlang-init))
;; (add-to-list 'flymake-allowed-file-name-masks '("\\.hrl\\'" flymake-erlang-init))
;; ;; Distel
;; (add-to-list 'load-path (fullpath-relative-to-current-file "distel_erlang/elisp"))
;; (require 'distel)
;; (distel-setup)

;; ,----
;; |
;; | Ada-mode
;; |
;; `----

;; Flymake for Ada
(require 'flymake)
(defun flymake-ada-init ()
  (flymake-simple-make-init-impl
   'flymake-create-temp-with-folder-structure nil nil
   buffer-file-name
   'flymake-get-ada-cmdline))

(defun flymake-get-ada-cmdline (source base-dir)
  `("gnatmake" ("-gnatc" "-gnatwa" ,(concat "-I" (expand-file-name base-dir)) ,source)))

(push '(".+\\.adb$" flymake-ada-init) flymake-allowed-file-name-masks)
(push '(".+\\.ads$" flymake-ada-init) flymake-allowed-file-name-masks)

(push '("\\([^:]*\\):\\([0-9]+\\):[0-9]+: \\(.*\\)"
        1 2 nil 3)
      flymake-err-line-patterns)
;; ,----
;; |
;; | cflow
;; | - EXTERNAL REQUIREMENT: cflow
;; |
;; `----
;; (load (fullpath-relative-to-current-file "cflow-mode"))
;; (autoload 'cflow-mode "cflow-mode")
;; (setq auto-mode-alist (append auto-mode-alist
;; '(("\\.cflow$" . cflow-mode)))) ;
;; ;;scala
;; ;; load the ensime lisp code...
;; (add-to-list 'load-path "~/.emacs.d/ensime/src/main/elisp/")
;; (require 'ensime)

;; This step causes the ensime-mode to be started whenever
;; scala-mode is started for a buffer. You may have to customize this step
;; if you're not using the standard scala mode.
;; (add-hook 'scala-mode-hook 'ensime-scala-mode-hook)

;; ,----
;; |
;; | Prolog-mode
;; |
;; `----
;; (load (fullpath-relative-to-current-file "prolog"))
(setq prolog-program-name "~/BProlog/bp")
(autoload 'run-prolog "prolog" "Start a Prolog sub-process." t)
(autoload 'prolog-mode "prolog" "Major mode for editing Prolog programs." t)
(autoload 'mercury-mode "prolog" "Major mode for editing Mercury programs." t)
(setq prolog-system 'swi)  ; optional, the system you are using;
                                        ; see `prolog-system' below for possible values
(setq auto-mode-alist (append '(("\\.pl$" . prolog-mode)
                                ("\\.m$" . mercury-mode)) auto-mode-alist))

(setq prolog-indent-width 4
      ;;             prolog-electric-dot-flag t
      ;;             prolog-electric-dash-flag t
      prolog-electric-colon-flag t)
(setq prolog-system 'swi
      prolog-program-switches '((swi ("-G128M" "-T128M" "-L128M" "-O"))
                                (t nil))
      prolog-electric-if-then-else-flag t)


;;prolog-flymake
(add-hook 'prolog-mode-hook
          (lambda ()
            (require 'flymake)
            (make-local-variable 'flymake-allowed-file-name-masks)
            (make-local-variable 'flymake-err-line-patterns)
            (setq flymake-err-line-patterns
                  '(("ERROR: (?\\(.*?\\):\\([0-9]+\\)" 1 2)
                    ("Warning: (\\(.*\\):\\([0-9]+\\)" 1 2)))
            (setq flymake-allowed-file-name-masks
                  '(("\\.pl\\'" flymake-prolog-init)))
            (flymake-mode 1)))

(defun flymake-prolog-init ()
  (let* ((temp-file   (flymake-init-create-temp-buffer-copy
                       'flymake-create-temp-inplace))
         (local-file  (file-relative-name
                       temp-file
                       (file-name-directory buffer-file-name))))
    (list "prolog" (list "-q" "-t" "halt" "-s " local-file))))


;;rebind ruby mode key
;; (eval-after-load 'ruby-mode
;;   '(progn
;;      (define-key ruby-mode-map (kbd "RET") 'reindent-then-newline-and-indent)
;;      (define-key ruby-mode-map (kbd "TAB") 'indent-for-tab-command)))

;;octave
(autoload 'octave-mode "octave-mode" nil t)
(setq auto-mode-alist
      (cons '("\\.m$" . octave-mode) auto-mode-alist))
(add-hook 'octave-mode-hook
          (lambda ()
            (abbrev-mode 1)
            (auto-fill-mode 1)
            (if (eq window-system 'x)
                (font-lock-mode 1))))

;; (require 'ac-octave)
;; (defun ac-octave-mode-setup ()
;;   (setq ac-sources '(ac-source-octave)))
;; (add-hook 'octave-mode-hook
;;           '(lambda () (ac-octave-mode-setup)))


;;emacs-ctable
(require 'ctable)

;;(eval-after-load "python"
;;  '(define-key python-mode-map "C-m" 'newline-and-indent))
;; (eval-after-load "python"
;;   '(define-key python-mode-map "M-e" 'forward-sentence))

;;emacs-eclim
;; (add-to-list 'load-path (fullpath-relative-to-current-file "emacs-eclim"))
(require 'eclim)
(global-eclim-mode)
(require 'eclimd)

(setq eclim-auto-save t
      eclim-executable "~/programs/eclipse/eclim"
      eclimd-executable "~/programs/eclipse/eclimd"
      eclimd-wait-for-process "eclimd"
      nil-default-workspace "~/src/Workspace"
      eclim-use-yasnippet nil
      help-at-pt-display-when-idle t
      help-at-pt-timer-delay 0.1)

(setq help-at-pt-display-when-idle t)
(setq help-at-pt-timer-delay 0.1)
(help-at-pt-set-timer)

;; (ac-emacs-eclim-config)

(define-key java-mode-map  [(control tab)] 'eclim-complete)
(define-key java-mode-map  [(tab)] 'ac-complete-with-helm)

;; (require 'company)
;; (require 'company-emacs-eclim)
;; (company-emacs-eclim-setup)
;; (defun my-company-eclim ()
;;   (interactive)
;;   (god-local-mode 0)
;;   (call-interactively 'company-emacs-eclim))
;; (define-key java-mode-map  [(tab)] 'my-company-eclim)

;;jdee
;; (add-to-list 'load-path "~/.emacs.d/jdee-2.4.1/lisp/")
;; (autoload 'jde-mode "jde" "JDE mode" t)
;; (setq auto-mode-alist
;;       (append '(("\\.java\\'" . jde-mode)) auto-mode-alist))
;; (push 'jde-mode ac-modes)
;; (add-hook 'jde-mode-hook (lambda () (push 'ac-source-semantic ac-sources)))
;;autogenerated code for color-theme-sanityinc-tomorrow-night

(provide 'setup-programming-modes)
