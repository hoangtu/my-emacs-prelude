(setq inhibit-splash-screen t)
;; (require 'bookmark)
(require 'bookmark+)
(bookmark-bmenu-list)
(switch-to-buffer "*Bookmark List*")
(setq bookmark-save-flag 1)

(provide 'setup-bookmark)
