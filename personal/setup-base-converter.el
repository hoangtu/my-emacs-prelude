(defun value-at-point (base-src base-target)
  (interactive )

  (let (inputStr tempStr p1 p2 )
    (save-excursion
      (search-backward-regexp "[^0-9A-Fa-fxb #]" nil t)
      (forward-char)
      (setq p1 (point) )
      (search-forward-regexp "[^0-9A-Fa-fxb #]" nil t)
      (backward-char)
      (setq p2 (point) ))

    (setq inputStr (buffer-substring-no-properties p1 p2) )

    (let ((case-fold-search nil))
      (setq base-fmt (cond ((eql base-src 16) "x")
                           ((eql base-src 10) "d")
                           ((eql base-src 2) "b")))
      (setq tempStr (replace-regexp-in-string (concat "^0" base-fmt) "" inputStr)) ; C, Perl, …
      (setq tempStr (replace-regexp-in-string (concat "^0" base-fmt) "" inputStr)) ; elisp …
      (setq tempStr (replace-regexp-in-string (concat "^0" base-fmt) "" inputStr))  ; CSS …
      (setq target-display-fmt (if (eql 16 base-target) "%x" "%d"))
      (setq base-name (cond ((eql base-src 16) "Hex")
                            ((eql base-src 10) "Dec")
                            ((eql base-src 2) "Bin")))
      
      (message (concat base-name " %s is %s")
               tempStr
               (format target-display-fmt (string-to-number (replace-regexp-in-string "[ ]+" "" tempStr) base-src))))))

(defun hex-to-dec ()
  "0xff"
  (interactive)
  (value-at-point 16 10))

(defun bin-to-dec ()
  "0b1111 1111"
  (interactive)
  (value-at-point 2 10))

(defun dec-to-hex ()
  "123"
  (interactive)
  (value-at-point 10 16))

;; (defun dec-to-bin ()
;;   "123"
;;   (interactive)
;;   (value-at-point 10 2))

;; (global-set-key (kbd "C-c d h d") 'hex-to-dec)
;; (global-set-key (kbd "C-c d b d") 'bin-to-dec)
;; (global-set-key (kbd "C-c d d h") 'dec-to-hex)
;; (global-set-key (kbd "C-c d h") 'hex-to-dec)

(provide 'setup-base-converter)
