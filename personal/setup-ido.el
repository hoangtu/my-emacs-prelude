;; Interactively Do Things - Copied from Mangars, author of Emacsrock

;; flx-ido looks better with ido-vertical-mode
(require 'ido-vertical-mode)
(ido-vertical-mode)

(require 'dash)

;; Always rescan buffer for imenu
(set-default 'imenu-auto-rescan t)

(add-to-list 'ido-ignore-directories "target")
(add-to-list 'ido-ignore-directories "node_modules")

;; Ido at point (C-,)
;; (require 'ido-at-point)
;; (ido-at-point-mode)

;; Use ido everywhere
;; (require 'ido-ubiquitous)
;; (ido-ubiquitous-mode 1)

;; Fix ido-ubiquitous for newer packages
(defmacro ido-ubiquitous-use-new-completing-read (cmd package)
  `(eval-after-load ,package
	 '(defadvice ,cmd (around ido-ubiquitous-new activate)
		(let ((ido-ubiquitous-enable-compatibility nil))
		  ad-do-it))))

(ido-ubiquitous-use-new-completing-read webjump 'webjump)
(ido-ubiquitous-use-new-completing-read yas-expand 'yasnippet)
(ido-ubiquitous-use-new-completing-read yas-visit-snippet-file 'yasnippet)

;; (defun ido-dired ()
;;   "Call 'diredp-dired-files the ido way.
;;     The directory is selected interactively by typing a substring.
;;     For details on keybindings, see `ido-find-file'."
;;   (interactive)
;;   (let ((ido-report-no-match nil)
;;         (ido-auto-merge-work-directories-length -1))
;;     (ido-file-internal 'diredp-dired-files 'diredp-dired-files nil "Dired: " 'dir)))

(provide 'setup-ido)
