(require 'eshell)
(require 'em-alias)

(defmacro with-face (str &rest properties)
  `(propertize ,str 'face (list ,@properties)))

(defun curr-dir-git-branch-string (pwd)
  "Returns current git branch as a string, or the empty string if
PWD is not in a git repo (or the git command is not found)."
  (interactive)
  (when (and (eshell-search-path "git")
             (locate-dominating-file pwd ".git"))
    (let ((git-output (shell-command-to-string (concat "cd " pwd " && git branch | grep '\\*' | sed -e 's/^\\* //'"))))
      (concat "["
              (if (> (length git-output) 0)
                  (substring git-output 0 -1)
                "(no branch)")
              "]")
      )))

(defun curr-dir-git-branch-string (pwd)
  "Returns current git branch as a string, or the empty string if
PWD is not in a git repo (or the git command is not found)."
  (interactive)
  (when (and (eshell-search-path "git")
             (locate-dominating-file pwd ".git"))
    (let ((git-output (shell-command-to-string (concat "cd " pwd " && git branch | grep '\\*' | sed -e 's/^\\* //'"))))
      (propertize (concat "["
                          (if (> (length git-output) 0)
                              (substring git-output 0 -1)
                            "(no branch)")
                          "]") 'face `(:foreground "green"))
      )))


(defun shk-eshell-prompt ()
  (let ((header-bg "pink"))
    (concat
     ;; (with-face "\n" ;; :background header-bg
     (with-face user-login-name :foreground "orange")
     (with-face "@" :foreground "orange")
     (with-face system-name :foreground "orange")
     (with-face (concat " "(eshell/pwd) " ") :foreground "yellow")
     (with-face (format-time-string "(%Y-%m-%d %H:%M) " (current-time))
                :foreground "#ADD8E6")
     ;; (with-face (or (ignore-errors (format "(%s)" (vc-responsible-backend default-directory))) ""))
     (or (curr-dir-git-branch-string (eshell/pwd)))
     "\n"
     (if (= (user-uid) 0)
         (with-face " #" :foreground "red")
       " $ ")
     )))

(setq eshell-prompt-function 'shk-eshell-prompt)
(setq eshell-highlight-prompt nil)

;; eshell customization
(add-hook
 'eshell-mode-hook
 (lambda ()
   (setq pcomplete-cycle-completions nil)))

;; (add-hook 'eshell-mode-hook
;;           (lambda ()
;;             (local-set-key (kbd "C-c h")
;;                            (lambda ()
;;                              (interactive)
;;                              (insert
;;                               (ido-completing-read "Eshell history: "
;;                                                    (delete-dups
;;                                                     (ring-elements eshell-history-ring))))))
;;             (local-set-key (kbd "C-c C-h") 'eshell-list-history)))

(require 'eshell)
;; (require 'em-smart)
;; (setq eshell-where-to-jump 'begin)
;; (setq eshell-review-quick-commands nil)
;; (setq eshell-smart-space-goes-to-end t)
;; (eshell-smart-initialize)

;; (defun open-file-at-cursor ()
;;   "Open the file path under cursor.
;; If there is text selection, uses the text selection for path.
;; If the path is starts with “http://”, open the URL in browser.
;; Input path can be {relative, full path, URL}.
;; This command is similar to `find-file-at-point' but without prompting for confirmation.
;; "
;;   (interactive)
;;   (let ( (path (if (region-active-p)
;;                    (buffer-substring-no-properties (region-beginning) (region-end))
;;                  (thing-at-point 'filename) ) ))
;;     (if (string-match-p "\\`https?://" path)
;;         (browse-url path)
;;       (progn ; not starting “http://”
;;         (if (file-exists-p path)
;;             (find-file path)
;;           (if (file-exists-p (concat path ".el"))
;;               (find-file (concat path ".el"))
;;             (when (y-or-n-p (format "file doesn't exist: 「%s」. Create?" path) )
;;               (find-file path ))))))))
;; (global-set-key (kbd "C-c f") 'open-file-at-cursor)

;; (load (fullpath-relative-to-current-file "my_alias"))

;;eshell auto jump
;; (load (fullpath-relative-to-current-file "eshell-autojump.el"))

(require 'eshell-autojump nil t)
(eshell-toggle-direct-send)
(company-mode -1)
(when (not (eq system-type 'windows-nt))
  (eshell/alias "ls" "ls --color -h --group-directories-first $*")
  (setq dired-listing-switches "-lha --group-directories-first"))

(eshell/alias "o" "")
(eshell/alias "o" "find-file-other-window $*")
(eshell/alias "vi" "find-file-other-window $*")
(eshell/alias "vim" "find-file-other-window $*")
(eshell/alias "emacs" "find-file-other-window $*")
(eshell/alias "em" "find-file-other-window $*")


(add-hook 'eshell-mode-hook 'rainbow-delimiters-mode)
(global-set-key (kbd "C-]") 'self-insert-command)
(global-set-key (kbd "M-]") 'abort-recursive-edit)

;; set PATH
(getenv "PATH")

(setenv "PATH"
        (concat
         "~/.emacs.d/personal/fasd/bin/"
         (getenv "PATH")))

(defadvice find-file (around find-files activate)
  "Also find all files within a list of files. This even works recursively."
  (if (listp filename)
      (loop for f in filename do (find-file-other-window f wildcards))
    ad-do-it))

(defadvice find-file-other-window (around find-files activate)
  "Also find all files within a list of files. This even works recursively."
  (if (listp filename)
      (loop for f in filename do (find-file-other-window f wildcards))
    ad-do-it))

(add-hook 'eshell-mode-hook 'smartparens-mode) ;

;;readline-complete
;; (setq explicit-shell-file-name "bash")
;; (setq explicit-bash-args '("-c" "export EMACS=; stty echo; bash"))
;; (setq comint-process-echoes t)
;; (require 'readline-complete)
;; (add-to-list 'ac-modes 'shell-mode)
;; (add-hook 'shell-mode-hook 'ac-rlc-setup-sources)

(add-hook 'eshell-mode-hook
          '(lambda ()
              (define-key eshell-mode-map [remap eshell-pcomplete] 'helm-esh-pcomplete)
              (define-key eshell-mode-map (kbd "M-l") 'helm-eshell-history)))

(require 'esh-buf-stack)
(setup-eshell-buf-stack)
(add-hook 'eshell-mode-hook
          (lambda ()
            (local-set-key
             (kbd "M-q") 'eshell-push-command)))

(provide 'setup-eshell)
