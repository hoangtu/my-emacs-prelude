(require 'popwin)
(popwin-mode 1)

(global-set-key (kbd "C-z") popwin:keymap)

;; setup in personal/custom.el
;; (add-to-list 'popwin:special-display-config '("*Completions*" :noselect t :position right))
;; (add-to-list 'popwin:special-display-config '("*compilation*" :noselect nil :position right))
;; (add-to-list 'popwin:special-display-config '("*Google Translate*" :noselect t :position))
;; (add-to-list 'popwin:special-display-config `("*Help*" :noselect nil :position top :height 25))
;; (add-to-list 'popwin:special-display-config '("*vc-diff*.*" :regexp t :noselect t :position top :height 35))
;; (add-to-list 'popwin:special-display-config '(" widget-choose" :noselect nil :position bottom))
;; (add-to-list 'popwin:special-display-config '("^\*helm.+\*$" :regexp t :height 15))

;; M-x dired-jump-other-window
;; (push '(dired-mode :position other) popwin:special-display-config)

;; M-!
;; (push "*Shell Command Output*" popwin:special-display-config)

;; M-x compile
;; (push '(compilation-mode :noselect t) popwin:special-display-config)

;; (push "*slime-apropos*" popwin:special-display-config)
;; (push "*slime-macroexpansion*" popwin:special-display-config)
;; (push "*slime-description*" popwin:special-display-config)
;; (push '("*slime-compilation*" :noselect t) popwin:special-display-config)
;; (push "*slime-xref*" popwin:special-display-config)
;; (push '(sldb-mode :stick t) popwin:special-display-config)
;; (push 'slime-connection-list-mode popwin:special-display-config)

(setq popwin:special-display-config
      '((semantic-symref-results-mode :position right :stick t)
        ("^*Symref.*" :regexp t :position right :stick t)
        ("*quickrun*")
        (" *undo-tree*" :width 0.3 :position right)
        ("*vc-change-log*")
        (slime-connection-list-mode)
        (sldb-mode :stick t)
        ("*slime-xref*")
        ("*slime-compilation*" :noselect t)
        ("*slime-description*")
        ("*slime-macroexpansion*")
        ("*slime-apropos*")
        (compilation-mode :noselect t)
        ("*Shell Command Output*")
        ("*Async Shell Command*")
        (" widget-choose" :position bottom :noselect nil)
        ("*Help*" :width 80 :position right :noselect nil :stick t)
        ("*Google Translate*" :noselect t :position nil)
        ("*compilation*" :position right :noselect nil)
        ("*Completions*" :position right :noselect t)
        ("*Miniedit Help*" :noselect t)
        (help-mode)
        (completion-list-mode :noselect t)
        (compilation-mode :noselect t)
        (grep-mode :noselect t)
        (occur-mode :noselect t)
        ("*Pp Macroexpand Output*" :noselect t)
        ("*Shell Command Output*")
        ("*vc-change-log*")
        (" *undo-tree*" :width 60 :position right)
        ("^\\*anything.*\\*$" :regexp t)
        ("*slime-apropos*")
        ("*slime-macroexpansion*")
        ("*slime-description*")
        ("*slime-compilation*" :noselect t)
        ("*slime-xref*")
        (sldb-mode :stick t)
        (slime-repl-mode)
        (slime-connection-list-mode)
        ))

;; undo-tree
;; (push '(" *undo-tree*" :width 0.3 :position right) popwin:special-display-config);

(provide 'setup-popwin)
