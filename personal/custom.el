(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ac-auto-start nil)
 '(ac-etags-requires 1)
 '(ansi-color-faces-vector
   [default bold shadow italic underline bold bold-italic bold])
 '(ansi-color-names-vector
   (vector "#cccccc" "#f2777a" "#99cc99" "#ffcc66" "#6699cc" "#cc99cc" "#66cccc" "#2d2d2d"))
 '(bmkp-bmenu-state-file "~/.emacs.d/savefile/bookmark/.emacs-bmk-bmenu-state.el")
 '(bmkp-last-as-first-bookmark-file "~/bookmarks/bookmarks")
 '(bmkp-prompt-for-tags-flag t)
 '(bookmark-automatically-show-annotations nil)
 '(bookmark-default-file "~/bookmarks/bookmarks")
 '(browse-url-browser-function (quote eww-browse-url))
 '(comint-prompt-read-only t)
 '(company-c-headers-path-system
   (quote
    ("/usr/include/" "/usr/local/include/" "/usr/include/c++/4.8.2/")))
 '(company-minimum-prefix-length 1)
 '(custom-enabled-themes (quote (sanityinc-tomorrow-bright)))
 '(custom-safe-themes
   (quote
    ("1b8d67b43ff1723960eb5e0cba512a2c7a2ad544ddb2533a90101fd1852b426e" default)))
 '(dired-listing-switches "-lha --group-directories-first")
 '(easy-kill-unhighlight-key [return])
 '(fci-rule-color "orange")
 '(find-tag-marker-ring-length 1000)
 '(flyspell-lazy-disallow-buffers (quote ("\\`[ *]" "\\`dmesg" "\\`message")))
 '(flyspell-lazy-idle-seconds 1.5)
 '(fuzzy-format-auto-indent t)
 '(ggtags-process-environment (quote ("GTAGSLIBPATH=/home/xtuudoo/.gtags")))
 '(global-mark-ring-max 5000)
 '(god-exempt-major-modes
   (quote
    (Man-mode Info-mode shell-mode eshell-mode magit-mode dired-mode git-commit-mode grep-mode magit-status-mode magit-log-edit-mode vc-annotate-mode slime-repl-mode Custom-mode term-mode eww-mode)))
 '(golden-ratio-exclude-modes
   (quote
    ("ediff-mode" "gdb-locals-mode" "gdb-breakpoints-mode" "gdb-frames-mode" "gud-mode" "gdb-inferior-io-mode" "magit-log-mode" "magit-reflog-mode" "magit-status-mode" "IELM" "eshell-mode" "dired-mode")))
 '(google-translate-default-source-language "en")
 '(google-translate-default-target-language "vi")
 '(helm-buffers-fuzzy-matching t)
 '(helm-command-prefix-key "C-c h")
 '(helm-dictionary-database "~/.emacs.d/en_vi.ding")
 '(helm-dictionary-online-dicts
   (quote
    (("en.wiktionary.org" . "http://en.wiktionary.org/wiki/%s")
     ("vdict" . "http://vdict.com/%s,1,0,0.html")
     ("Vietnamese Wikitionary" . "http://vi.wiktionary.org/wiki/%s")
     ("Dictionary.com" . "http://dictionary.reference.com/browse/%s")
     ("One Look" . "http://www.onelook.com/?w=%s"))))
 '(helm-gtags-auto-update t)
 '(helm-gtags-ignore-case t)
 '(helm-gtags-path-style (quote relative))
 '(helm-idle-delay 0.0)
 '(helm-mini-default-sources
   (quote
    (helm-source-buffers-list helm-source-bookmarks helm-source-recentf helm-source-buffer-not-found)))
 '(helm-sources-using-default-as-input
   (quote
    (helm-source-imenu helm-source-semantic helm-source-info-elisp helm-source-etags-select helm-source-occur helm-source-man-pages)))
 '(helm-split-window-default-side (quote other))
 '(highlight-symbol-idle-delay 1)
 '(iflipb-wrap-around t)
 '(large-file-warning-threshold 100000000)
 '(linum-relative-format " %3s")
 '(nyan-bar-length 20)
 '(org-drill-optimal-factor-matrix (quote ((1 (1.96 . 3.58)))))
 '(persp-nil-name "none")
 '(projectile-switch-project-action (quote projectile-dired))
 '(send-mail-function (quote smtpmail-send-it))
 '(shell-pop-shell-type (quote ("eshell" "*eshell*" (lambda nil (eshell)))))
 '(tab-width 4)
 '(tcl-application "tclsh")
 '(vc-annotate-background nil)
 '(vc-annotate-color-map
   (quote
    ((20 . "#f2777a")
     (40 . "#f99157")
     (60 . "#ffcc66")
     (80 . "#99cc99")
     (100 . "#66cccc")
     (120 . "#6699cc")
     (140 . "#cc99cc")
     (160 . "#f2777a")
     (180 . "#f99157")
     (200 . "#ffcc66")
     (220 . "#99cc99")
     (240 . "#66cccc")
     (260 . "#6699cc")
     (280 . "#cc99cc")
     (300 . "#f2777a")
     (320 . "#f99157")
     (340 . "#ffcc66")
     (360 . "#99cc99"))))
 '(vc-annotate-very-old-color nil)
 '(vlf-application (quote dont-ask))
 '(vlf-batch-size 500000)
 '(win-switch-idle-time 1)
 '(woman-use-topic-at-point t)
 '(yas-snippet-dirs
   (quote
    ("~/.emacs1.d/snippets" "/home/xtuudoo/.emacs1.d/elpa/yasnippet-20140514.1649/snippets")) nil (yasnippet)))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(cfw:face-toolbar-button-off ((t (:foreground "light gray" :weight bold))))
 '(cfw:face-toolbar-button-on ((t (:foreground "lawn green" :weight bold))))
 '(company-scrollbar-bg ((t (:background "#191919"))))
 '(company-scrollbar-fg ((t (:background "#0c0c0c"))))
 '(company-tooltip ((t (:inherit default :background "#050505"))))
 '(company-tooltip-common ((t (:inherit font-lock-constant-face))))
 '(company-tooltip-selection ((t (:inherit font-lock-function-name-face))))
 '(easy-kill-selection ((t (:inherit secondary-selection :background "blue"))))
 '(fa-face-hint-bold ((t (:background "dark slate blue" :weight bold))))
 '(helm-ff-directory ((t nil)))
 '(helm-selection ((t (:inherit highlight :underline t))))
 '(italic ((t (:slant italic :family "Inconsolata-Italic"))))
 '(persp-selected-face ((t (:foreground "yellow" :weight bold))))
 '(region ((t (:background "blue"))))
 '(shr-link ((t (:foreground "dark orange" :underline t))))
 '(whitespace-empty ((t (:background "yellow" :foreground "DarkOrange4" :inverse-video t :underline nil))))
 '(whitespace-hspace ((t (:foreground "sandy brown"))))
 '(whitespace-newline ((t (:foreground "green yellow" :weight normal))))
 '(whitespace-space ((t (:foreground "sandy brown"))))
 '(whitespace-space-after-tab ((t (:background "yellow" :foreground "DarkOrange4" :inverse-video t :underline nil))))
 '(whitespace-space-before-tab ((t (:background "yellow" :foreground "DarkOrange4" :inverse-video t :underline nil))))
 '(whitespace-tab ((t (:foreground "LightGoldenrod1")))))
