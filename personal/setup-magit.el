(require 'magit)

;; C-x C-k to kill file on line
;; (set-face-background 'magit-item-highlight "#121212")
;; (set-face-background 'diff-file-header "#121212")
;; (set-face-foreground 'diff-context "#666666")
;; (set-face-foreground 'diff-added "light green")
;; (set-face-foreground 'diff-removed "light pink")

(add-hook 'git-commit-mode-hook (lambda () (toggle-save-place 0)))

(set-default 'magit-stage-all-confirm nil)

;; (defun magit-just-amend ()
;;   (interactive)
;;   (save-window-excursion
;;     (magit-with-refresh
;;       (shell-command "git --no-pager commit --amend --reuse-message=HEAD"))))

(defun magit-kill-file-on-line ()
  "Show file on current magit line and prompt for deletion."
  (interactive)
  (magit-visit-item)
  (delete-current-buffer-file)
  (magit-refresh))

(add-hook 'magit-mode-hook 'magit-load-config-extensions)

;; C-x C-k to kill file on line
(defun magit-kill-file-on-line ()
  "Show file on current magit line and prompt for deletion."
  (interactive)
  (magit-visit-item)
  (delete-current-buffer-file)
  (magit-refresh))

;; full screen magit-status

(defadvice magit-status (around magit-fullscreen activate)
  (window-configuration-to-register :magit-fullscreen)
  ad-do-it
  (delete-other-windows))

(defun magit-quit-session ()
  "Restores the previous window configuration and kills the magit buffer"
  (interactive)
  (kill-buffer)
  (jump-to-register :magit-fullscreen))

;; full screen vc-annotate

(defun vc-annotate-quit ()
  "Restores the previous window configuration and kills the vc-annotate buffer"
  (interactive)
  (kill-buffer)
  (jump-to-register :vc-annotate-fullscreen))

(eval-after-load "vc-annotate"
  '(progn
     (defadvice vc-annotate (around fullscreen activate)
       (window-configuration-to-register :vc-annotate-fullscreen)
       ad-do-it
       (delete-other-windows))

     (define-key vc-annotate-mode-map (kbd "q") 'vc-annotate-quit)))

;; ignore whitespace

(defun magit-toggle-whitespace ()
  (interactive)
  (if (member "-w" magit-diff-options)
      (magit-dont-ignore-whitespace)
    (magit-ignore-whitespace)))

(defun magit-ignore-whitespace ()
  (interactive)
  (add-to-list 'magit-diff-options "-w")
  (magit-refresh))

(defun magit-dont-ignore-whitespace ()
  (interactive)
  (setq magit-diff-options (remove "-w" magit-diff-options))
  (magit-refresh))

(define-key magit-status-mode-map (kbd "W") 'magit-toggle-whitespace)

(add-hook 'git-commit-mode-hook 'magit-commit-mode-init)
(add-hook 'magit-mode-hook (lambda () (linum-mode -1)))

;; close popup when commiting

(defadvice git-commit-commit (after delete-window activate)
  (delete-window))

(require 'magit-commit-training-wheels)
(ad-activate 'magit-log-edit-commit)

(provide 'setup-magit)
