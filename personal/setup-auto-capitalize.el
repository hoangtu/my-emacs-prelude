(autoload 'auto-capitalize-mode "auto-capitalize"
  "Toggle `auto-capitalize' minor mode in this buffer." t)
(autoload 'turn-on-auto-capitalize-mode "auto-capitalize"
  "Turn on `auto-capitalize' minor mode in this buffer." t)
(autoload 'enable-auto-capitalize-mode "auto-capitalize"
  "Enable `auto-capitalize' minor mode in this buffer." t)

(add-hook 'text-mode-hook 'turn-on-auto-capitalize-mode)
(add-hook 'text-mode-hook 'enable-auto-capitalize-mode)
(add-hook 'org-mode-hook 'turn-on-auto-capitalize-mode)
(add-hook 'org-mode-hook 'enable-auto-capitalize-mode)

(provide 'setup-auto-capitalize)
