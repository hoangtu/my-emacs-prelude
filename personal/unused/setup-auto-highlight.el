(global-auto-highlight-symbol-mode)

(define-key auto-highlight-symbol-mode-map (kbd "M-p") 'ahs-backward)
(define-key auto-highlight-symbol-mode-map (kbd "M-n") 'ahs-forward)
(define-key auto-highlight-symbol-mode-map (kbd "M-P") 'ahs-backward-definition)
(define-key auto-highlight-symbol-mode-map (kbd "M-S") 'ahs-forward-definition)
(define-key auto-highlight-symbol-mode-map (kbd "C-c x") 'ahs-back-to-start)
(define-key auto-highlight-symbol-mode-map (kbd "C-x C-'") 'ahs-change-range)
(define-key auto-highlight-symbol-mode-map (kbd "C-x C-a") 'ahs-edit-mode)

(provide 'setup-ahs)
