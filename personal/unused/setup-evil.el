(require 'evil)
(require 'evil-visualstar)
(require 'evil-leader)
(require 'surround)

(evil-mode 1)
(global-evil-leader-mode)
(global-surround-mode 1)

;; Note: lexical-binding must be t in order for this to work correctly.
;; (defun make-conditional-key-translation (key-from key-to translate-keys-p)
;;   "Make a Key Translation such that if the translate-keys-p function returns true,
;;    key-from translates to key-to, else key-from translates to itself.  translate-keys-p
;;    takes key-from as an argument. "
;;   (define-key key-translation-map key-from
;;     (lambda (prompt)
;;       (if (funcall translate-keys-p key-from) key-to key-from))))
;; (defun my-translate-keys-p (key-from)
;;   "Returns whether conditional key translations should be active.  See make-conditional-key-translation function. "
;;   (and
;;    ;; Only allow a non identity translation if we're beginning a Key Sequence.
;;    (equal key-from (this-command-keys))
;;    (or (evil-motion-state-p) (evil-normal-state-p) (evil-visual-state-p))))

;; (define-key evil-normal-state-map "c" nil)
;; (define-key evil-motion-state-map "cu" 'universal-argument)
;; (make-conditional-key-translation (kbd "ch") (kbd "C-h") 'my-translate-keys-p)
;; (make-conditional-key-translation (kbd "g") (kbd "C-x") 'my-translate-keys-p)

;; evil surround setup
(add-hook 'c++-mode-hook (lambda ()
                           (push '(?< . ("< " . " >")) surround-pairs-alist)))
(add-hook 'emacs-lisp-mode-hook (lambda ()
                                  (push '(?` . ("`" . "'")) surround-pairs-alist)))
(add-to-list 'surround-operator-alist
             '(evil-paredit-change . change))
(add-to-list 'surround-operator-alist
             '(evil-paredit-delete . delete))

(evil-leader/set-key
  "e" 'helm-find-files
  "b" 'switch-to-buffer
  "V" 'evil-window-vsplit
  "v" 'evil-window-split
  "j" 'windmove-down
  "k" 'windmove-up
  "h" 'windmove-left
  "l" 'windmove-right)

;; (define-key evil-normal-state-map "c" nil)
;; (define-key evil-motion-state-map "cu" 'universal-argument)
(add-hook 'emacs-lisp-mode-hook 'evil-paredit-mode)

(key-chord-define evil-normal-state-map " w" #'ace-jump-word-mode)
(key-chord-define evil-visual-state-map " w" #'ace-jump-word-mode)

;; evil tab
(define-key evil-normal-state-map (kbd "C-t") 'whitespace-mode)
(define-key evil-normal-state-map (kbd "M-.") 'helm-etags-select)
(define-key evil-normal-state-map (kbd "C-w t") 'elscreen-create) ;creat tab
(define-key evil-normal-state-map (kbd "C-w x") 'elscreen-kill) ;kill tab
(define-key evil-normal-state-map "gT" 'elscreen-previous) ;previous tab
(define-key evil-normal-state-map "gt" 'elscreen-next) ;next tab

(global-set-key (kbd "M-;") 'evilnc-comment-or-uncomment-lines)
(global-set-key (kbd "C-;") 'evilnc-comment-or-uncomment-to-the-line)
(global-set-key (kbd "C-c w") 'evilnc-copy-to-line)
(global-set-key (kbd "C-c k") 'evilnc-kill-to-line)

(define-key evil-normal-state-map "g," 'evilnc-comment-or-uncomment-lines)
(define-key evil-normal-state-map "gl" 'evilnc-comment-or-uncomment-to-the-line)
(define-key evil-normal-state-map "gc" 'evilnc-copy-and-comment-lines)
;; (define-key evil-normal-state-map ",cp" 'evilnc-comment-or-uncomment-paragraphs)
;; (define-key evil-normal-state-map "gr" 'comment-or-uncomment-region)

(defmacro my-define-niv (in out) `(prog
                                    (define-key evil-normal-state-map ,in ,out)
                                    (define-key evil-insert-state-map ,in ,out)
                                    (define-key evil-visual-state-map ,in ,out)
                                    (define-key evil-motion-state-map ,in ,out)
                                    ))
(defmacro my-define-nv (in out) `(progn
                                   (define-key evil-normal-state-map ,in ,out)
                                   (define-key evil-visual-state-map ,in ,out)
                                   (define-key evil-motion-state-map ,in ,out)
                                   ))

(my-define-nv (kbd "grep") 'helm-do-grep)
(my-define-nv (kbd "go") 'helm-find-files)
(my-define-nv (kbd "gb") 'ido-switch-buffer)
(my-define-nv (kbd "gs") 'magit-status)
(my-define-nv (kbd "g.") 'ido-dired)
(my-define-nv (kbd ")") 'evil-next-close-paren)
(my-define-nv (kbd "(") 'evil-previous-open-paren)

;; preserve basic and useful Emacs key bindings

(define-key evil-normal-state-map "\C-a" 'prelude-move-beginning-of-line)
(define-key evil-insert-state-map "\C-a" 'prelude-move-beginning-of-line)
(define-key evil-visual-state-map "\C-a" 'prelude-move-beginning-of-line)
(define-key evil-motion-state-map "\C-a" 'prelude-move-beginning-of-line)
(define-key evil-normal-state-map "\C-e" 'evil-end-of-line)
(define-key evil-insert-state-map "\C-e" 'end-of-line)
(define-key evil-visual-state-map "\C-e" 'evil-end-of-line)
(define-key evil-motion-state-map "\C-e" 'evil-end-of-line)
(define-key evil-normal-state-map "\C-f" 'evil-forward-char)
(define-key evil-insert-state-map "\C-f" 'evil-forward-char)
(define-key evil-insert-state-map "\C-f" 'evil-forward-char)
(define-key evil-normal-state-map "\C-b" 'evil-backward-char)
(define-key evil-insert-state-map "\C-b" 'evil-backward-char)
(define-key evil-visual-state-map "\C-b" 'evil-backward-char)
(define-key evil-normal-state-map "\C-d" 'evil-scroll-down)
(define-key evil-normal-state-map "\C-u"'evil-scroll-up)
(define-key evil-insert-state-map "\C-d" 'evil-delete-char)
(define-key evil-visual-state-map "\C-d" 'evil-delete-char)
(define-key evil-normal-state-map "\C-n" 'evil-next-line)
(define-key evil-insert-state-map "\C-n" 'evil-next-line)
(define-key evil-visual-state-map "\C-n" 'evil-next-line)
(define-key evil-normal-state-map "\C-p" 'evil-previous-line)
(define-key evil-insert-state-map "\C-p" 'evil-previous-line)
(define-key evil-visual-state-map "\C-p" 'evil-previous-line)
(define-key evil-normal-state-map "\C-w" 'evil-delete)
(define-key evil-insert-state-map "\C-w" 'evil-delete)
(define-key evil-visual-state-map "\C-w" 'evil-delete)
(define-key evil-normal-state-map "\C-y" 'yank)
(define-key evil-insert-state-map "\C-y" 'yank)
(define-key evil-visual-state-map "\C-y" 'yank)
(define-key evil-normal-state-map "\C-k" 'kill-line)
(define-key evil-insert-state-map "\C-k" 'kill-line)
(define-key evil-visual-state-map "\C-k" 'kill-line)
(define-key evil-normal-state-map "Q" 'call-last-kbd-macro)
(define-key evil-visual-state-map "Q" 'call-last-kbd-macro)
(define-key evil-normal-state-map (kbd "TAB") 'evil-undefine)

(defun evil-undefine ()
  (interactive)
  (let (evil-mode-map-alist)
    (call-interactively (key-binding (this-command-keys)))))

;; (key-chord-define-global "jk"  'evil-normal-state)
(key-chord-define evil-normal-state-map "jk" 'evil-force-normal-state)
(key-chord-define evil-visual-state-map "jk" 'evil-change-to-previous-state)
(key-chord-define evil-insert-state-map "jk" 'evil-normal-state)
(key-chord-define evil-replace-state-map "jk" 'evil-normal-state)

(provide 'setup-evil)
