(require 'guide-key)
(setq guide-key/guide-key-sequence '("C-x r" "C-x 4" "C-x c" "C-c p" "C-x p" "C-x v" "C-x t" "C-x x" "C-c h" "C-c i" "C-c g" "C-c !" "C-c &" "C-c /" "C-M-m"))
(setq guide-key/highlight-command-regexp "register")
(setq guide-key/highlight-command-regexp "mark")
(guide-key-mode 1) ; Enable guide-key-mode

(defun guide-key/my-hook-function-for-org-mode ()
  (guide-key/add-local-guide-key-sequence "C-c")
  (guide-key/add-local-guide-key-sequence "C-c C-x")
  (guide-key/add-local-highlight-command-regexp "org-"))

(provide 'setup-guide-key)
