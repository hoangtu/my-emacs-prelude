(defun ido-sunrise ()
  "Call `sunrise' the ido way.
    The directory is selected interactively by typing a substring.
    For details on keybindings, see `ido-find-file'."
  (interactive)
  (let ((ido-report-no-match nil)
        (ido-auto-merge-work-directories-length -1))
    (ido-file-internal 'sr-dired 'sr-dired nil "Sunrise: " 'dir)))

;;(define-key (cdr (assoc 'ido-mode minor-mode-map-alist)) [remiredap dired] 'ido-sunrise)
(global-set-key (kbd "C-x d") 'ido-sunrise)
(eval-after-load 'sunrise-commander
  '(progn
     (sr-rainbow sr-gorw-dir-face
                 (:background "misty rose"
                              :foreground "blue1"
                              :bold t)
                 "^..\\(d....\\(...\\)?w..*$\\)")
     (sr-rainbow sr-gorw-face
                 (:background "misty rose")
                 "^..\\(-....\\(...\\)?w..*$\\)")))

(setq find-directory-functions (cons 'sr-dired find-directory-functions))
