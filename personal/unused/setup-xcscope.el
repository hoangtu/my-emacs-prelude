;;xcscope
(require 'xcscope)
(cscope-setup)
;; (setq cscope-do-not-update-database t)
(setq cscope-program "gtags-cscope")

(provide 'setup-xcscope)
