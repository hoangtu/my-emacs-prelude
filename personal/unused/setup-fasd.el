(require 'fasd)
(global-fasd-mode 1)
(global-set-key (kbd "C-h C-/") 'fasd-find-file)

(provide 'setup-fasd)
