;;highlight parentheses
;; (load (fullpath-relative-to-current-file "highlight-parentheses"))
(require 'highlight-parentheses)
(highlight-parentheses-mode)
(define-globalized-minor-mode global-highlight-parentheses-mode
  highlight-parentheses-mode
  (lambda ()
    (highlight-parentheses-mode t)))
(global-highlight-parentheses-mode t)

(provide 'setup-highlight-parentheses)
