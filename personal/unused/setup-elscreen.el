;; ;; ---------------------------------------
;; ;; load elscreen
;; ;; ---------------------------------------
(require 'elscreen)
(setq elscreen-prefix-key [(control \\)]) ;; must go before loading elscreen
(elscreen-start)
;; F9 creates a new elscreen, shift-F9 kills it
;;(global-set-key (kbd "C-c t a b e") 'elscreen-create)
;;(global-set-key (kbd "C-c t a b d") 'elscreen-kill)

(provide 'setup-elscreen)
