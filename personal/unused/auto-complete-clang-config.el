(require 'auto-complete-clang)
(require 'auto-complete-config)

(setq ac-auto-start nil)
(setq ac-quick-help-delay 0)

;; (defun my-ac-complete-semantic-with-helm ()
;;   (interactive)
;;   (call-interactively 'ac-complete-semantic)
;;   (call-interactively 'ac-complete-with-helm)
;;   ;; (call-interactively 'ac-complete-clang)
;;   )

(defun my-ac-complete-with-helm ()
  (interactive)
  ;; (call-interactively 'ac-complete-semantic)
  (call-interactively 'auto-complete)
  (call-interactively 'ac-complete-with-helm)
  )

;; (ac-set-trigger-key "TAB")
(define-key c-mode-map  [(control tab)] 'ac-complete-clang)
(define-key c++-mode-map  [(control tab)] 'ac-complete-clang)
;; (define-key c-mode-map  [(tab)] 'moo-complete)
;; (define-key c++-mode-map  [(tab)] 'moo-complete)

(defun my-ac-config ()
  (setq-default ac-sources '(ac-source-abbrev ac-source-dictionary ac-source-words-in-same-mode-buffers ac-etags))
  (add-hook 'emacs-lisp-mode-hook 'ac-emacs-lisp-mode-setup)
  (add-hook 'c-mode-common-hook 'ac-cc-mode-setup)
  (add-hook 'ruby-mode-hook 'ac-ruby-mode-setup)
  (add-hook 'css-mode-hook 'ac-css-mode-setup)
  (add-hook 'auto-complete-mode-hook 'ac-common-setup)
  (global-auto-complete-mode t))

(defun my-ac-cc-mode-setup ()
  (setq ac-sources (append '(ac-source-clang ac-source-yasnippet) ac-sources)))

(add-hook 'c-mode-common-hook 'my-ac-cc-mode-setup)

;; ac-source-gtags
(my-ac-config)

(setq ac-clang-flags
      (mapcar (lambda (item)(concat "-I" item))
              (split-string
               "
 /usr/local/lib/gcc/x86_64-unknown-linux-gnu/4.8.1/../../../../include/c++/4.8.1
 /usr/local/lib/gcc/x86_64-unknown-linux-gnu/4.8.1/../../../../include/c++/4.8.1/x86_64-unknown-linux-gnu/.
 /usr/local/lib/gcc/x86_64-unknown-linux-gnu/4.8.1/../../../../include/c++/4.8.1/backward
 /usr/local/lib/gcc/x86_64-unknown-linux-gnu/4.8.1/include
 /usr/local/include
 /usr/local/lib/gcc/x86_64-unknown-linux-gnu/4.8.1/include-fixed
 /usr/include/x86_64-linux-gnu
 /usr/include
 /usr/include/c++/4.8
 /usr/include/x86_64-linux-gnu/c++/4.8
 /usr/include/c++/4.8/backward
 /usr/lib/gcc/x86_64-linux-gnu/4.8/include
 /usr/local/include
 /usr/lib/gcc/x86_64-linux-gnu/4.8/include-fixed
 /usr/include/x86_64-linux-gnu
 /usr/include

"
               )))
