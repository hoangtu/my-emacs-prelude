(require 'org-present)

(add-hook 'org-present-mode-hook
		  (lambda ()
			(org-present-big)
			(org-display-inline-images)))

(add-hook 'org-present-mode-quit-hook
		  (lambda ()
			(org-present-small)
			(org-remove-inline-images)))


(define-key org-present-mode-keymap [right]         'org-present-next)
(define-key org-present-mode-keymap [left]          'org-present-prev)
(define-key org-present-mode-keymap (kbd "C-c C-=") 'org-present-big)
(define-key org-present-mode-keymap (kbd "C-c C--") 'org-present-small)
(define-key org-present-mode-keymap (kbd "C-c C-q") 'org-present-quit)
(define-key org-present-mode-keymap (kbd "C-c C-r") 'org-present-read-only)
(define-key org-present-mode-keymap (kbd "C-c C-w") 'org-present-read-write)

(provide 'setup-org-present)
