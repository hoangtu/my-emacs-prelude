(require '123-menu)

(global-set-key "\M-m" '123-menu-display-menu-marc-menu-root)

(123-menu-defmenu marc-menu-root
                  ("n" "[N]avigation |" '123-menu-display-menu-marc-menu-fastnav)
                  ("m" "[M]arks |"     '123-menu-display-menu-marc-menu-marks)
                  ("w" "[W]indow |"    '123-menu-display-menu-marc-menu-window)
                  ("b" "[B]ookmarks |"    '123-menu-display-menu-menu-bookmark)
                  ("f" "[F]lyspell"    '123-menu-display-menu-menu-flyspell)
                  )

(123-menu-defmenu marc-menu-abbrev
                  ("g" "[G]lobal"              'add-global-abbrev)
                  ("m" "[M]ode-specific"       'add-mode-abbrev)
                  ("i" "[I]nverse"             '123-menu-display-menu-marc-menu-abbrev-inverse))

(123-menu-defmenu marc-menu-abbrev-inverse
                  ("g" "[G]lobal"              'inverse-add-global-abbrev)
                  ("m" "[M]ode-specific"       'inverse-add-mode-abbrev))

(123-menu-defmenu marc-menu-buffer
                  ("k" "[K]ill"                'kill-buffer)
                  ("l" "[L]ist"                'list-buffers)
                  ("r" "[R]ename"              'rename-buffer))

(123-menu-defmenu marc-menu-code
                  ("c" "[C]omment region"      'comment-region)
                  ("u" "[U]ncomment region"    'uncomment-region))

(123-menu-defmenu marc-menu-eval
                  ("b" "[B]uffer"              'eval-buffer)
                  ("l" "[L]ast sexp"           'eval-last-sexp)
                  ("r" "[R]egion"              'eval-region))

(123-menu-defmenu marc-menu-file
                  ("d" "[D]ired"               'dired)
                  ("f" "[F]ind"                'find-file)
                  ("i" "[I]nsert"              'insert-file)
                  ("s" "[S]ave"                'save-buffer))

(123-menu-defmenu marc-menu-marks
                  ("j" "[J]ump"                'bookmark-jump)
                  ("s" "[S]et"                 'bookmark-set))

(123-menu-defmenu marc-menu-rect
                  ("k" "[K]ill"                'kill-rectangle)
                  ("y" "[Y]ank"                'yank-rectangle)
                  ("o" "[O]pen"                'open-rectangle))

(123-menu-defmenu marc-menu-search
                  ("i" "[I]ncremental"         'isearch-forward)
                  ("o" "[O]ccur"               'occur))

(123-menu-defmenu marc-menu-version
                  ("a" "[A]nnotate"            'vc-annotate)
                  ("d" "[D]iff"                'vc-diff)
                  ("l" "[L]og"                 'vc-print-log))

(123-menu-defmenu marc-menu-window
                  ("1" "[1]window"             'delete-other-windows)
                  ("o" "[O]ther"               'other-window)
                  ("v" "[V]ert split"          'split-window-vertically)
                  ("h" "[H]oriz split"         'split-window-horizontally))

(123-menu-defmenu marc-menu-fastnav
                  ("" "" nil)
                  ("s" "[s]print forward |" 'fastnav-sprint-forward)
                  ("S" "[S]print backward\n" 'fastnav-sprint-backward)
                  ("m" "[m]ark forward |" 'fastnav-mark-to-char-forward)
                  ("M" "[M]ark backward\n" 'fastnav-mark-to-char-backward)
                  ("r" "[r]eplcen forward |" 'fastnav-replace-char-forward)
                  ("R" "[R]eplace backward\n" 'fastnav-replace-char-backward)
                  ("i" "[i]nsert forward |" 'fastnav-insert-at-char-forward)
                  ("I" "[I]nsert backward\n" 'fastnav-insert-at-char-backward)
                  ("d" "[d]elete forward |" 'fastnav-delete-char-forward)
                  ("D" "[D]elete backward" 'fastnav-delete-char-backward))

(123-menu-defmenu menu-bookmark
                  ("" "" nil)
                  ("a" "[a]dd file |" 'bookmark-set)
                  ("l" "[l]ist bookmarks" 'bookmark-bmenu-list)
                  )

(123-menu-defmenu menu-flyspell
                  ("" "" nil)
                  ("c" "Flyspell [c]orrect word |" ))

(123-menu-defmenu menu-magit
                  ("" "" nil))

(123-menu-defmenu menu-elscreen
                  ("" "" nil))
