(require 'cycbuf)

(global-set-key [(meta left)]        'cycbuf-switch-to-previous-buffer)
(global-set-key [(meta right)]        'cycbuf-switch-to-next-buffer)
(global-set-key [(meta super right)] 'cycbuf-switch-to-next-buffer-no-timeout)
(global-set-key [(meta super left)]  'cycbuf-switch-to-previous-buffer-no-timeout)

(provide 'setup-cycbuf)
