;;multiple-cursor
(require 'multiple-cursors)

(global-set-key (kbd "C-c C-c") 'mc/edit-lines)
(global-set-key (kbd "C-x >") 'mc/mark-next-like-this)
(global-set-key (kbd "C-x <") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-x <") 'mc/mark-all-like-this)

(define-key mc/keymap (kbd "C-c C-.") 'mc/remove-current-cursor)
;; (define-key mc/keymap (kbd "C-c ")   'mc/remove-duplicated-cursors)

(define-key mc/keymap (kbd "C-. =")   'mc/compare-chars)

;; (define-key cua--rectangle-keymap (kbd "C-. C-,") 'mc/cua-rectangle-to-multiple-cursors)
;; (mc/cua-rectangle-setup)

(provide 'setup-mc-and-cua)
