;;enable-flex-isearch
(defun flex-isearch-on ()
  (unless (minibufferp)
    (flex-isearch-mode 1)))

(define-globalized-minor-mode flex-isearch-global-mode
  flex-isearch-mode
  flex-isearch-on)

(flex-isearch-global-mode)
