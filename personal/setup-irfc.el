(require 'irfc)
(setq irfc-directory "~/.emacs.d/rfc")
(setq irfc-assoc-mode t)

(provide 'setup-irfc)
