;; ;;dired
;; (require 'dired-details)
;; (dired-details-install)
(require 'dired-dups)

;; allow dired to be able to delete or copy a whole dir.
(setq dired-recursive-copies (quote always)) ; “always” means no asking
(setq dired-recursive-deletes (quote top)) ; “top” means ask once

(add-hook 'dired-mode-hook 'auto-revert-mode)

(setq dired-dwim-target t)

(eval-after-load 'tramp
  '(progn
     ;; Allow to use: /sudo:user@host:/path/to/file
     (add-to-list 'tramp-default-proxies-alist
                  '(".*" "\\`.+\\'" "/ssh:%h:"))))

;; (defun dired-back-to-top ()
;;   (interactive)
;;   (beginning-of-buffer)
;;   (dired-next-line 4))

;; (define-key dired-mode-map
;;   (vector 'remap 'beginning-of-buffer) 'dired-back-to-top)

(defun dired-jump-to-bottom ()
  (interactive)
  (end-of-buffer)
  (dired-next-line -1))

(defun dired-insert-old-subdirs (old-subdir-alist)
  "Try to insert all subdirs that were displayed before.
Do so according to the former subdir alist OLD-SUBDIR-ALIST."
  (setq case-fold-search nil)
  (or (string-match "R" dired-actual-switches)
      (let (elt dir)
        (while old-subdir-alist
          (setq elt (car old-subdir-alist)
                old-subdir-alist (cdr old-subdir-alist)
                dir (car elt))
          (condition-case ()
              (progn
                (dired-uncache dir)
                (dired-insert-subdir dir))
            (error nil))))))

(define-key dired-mode-map
  (vector 'remap 'end-of-buffer) 'dired-jump-to-bottom)

(require 'dired+)

(define-key dired-mode-map (kbd "C-o") 'dired-display-file)
(define-key dired-mode-map (kbd "o") 'dired-find-file-other-window)

(provide 'setup-dired)
