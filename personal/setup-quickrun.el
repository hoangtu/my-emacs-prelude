(require 'quickrun)

;; (global-set-key (kbd "<f5>") 'quickrun)
;; (global-set-key (kbd "M-<f5>") 'quickrun-compile-only)

;; I recommend you set popwin for quickrun.el
;; See also http://www.emacswiki.org/emacs/PopWin
(push '("*quickrun*") popwin:special-display-config)

;; Use this parameter as C++ default
(quickrun-add-command "c++/c11"
                      '((:command . "g++")
                        (:exec    . ("%c -std=c++0x %o -o %e %s"
                                     "%e %a"))
                        (:remove  . ("%e")))
                      :default "c++")

;; Use this parameter in pod-mode
(quickrun-add-command "pod"
                      '((:command . "perldoc")
                        (:exec    . "%c -T -F %s"))
                      :mode 'pod-mode)

;; You can override existing command
;; (quickrun-add-command "c/gcc"
;;                       '((:exec . ("%c -std=c++0x %o -o %e %s"
;;                                   "%e %a")))
;; 					  :override t)

(provide 'setup-quickrun)
