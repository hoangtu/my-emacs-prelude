(autoload 'turn-on-ctags-auto-update-mode "ctags-update" "turn on `ctags-auto-update-mode'." t)
(add-hook 'c-mode-common-hook  'turn-on-ctags-auto-update-mode)
(add-hook 'c++-mode-common-hook  'turn-on-ctags-auto-update-mode)
(add-hook 'emacs-lisp-mode-hook  'turn-on-ctags-auto-update-mode)
(add-hook 'lisp-mode-hook  'turn-on-ctags-auto-update-mode)
(add-hook 'tcl-mode-hook  'turn-on-ctags-auto-update-mode)
(add-hook 'scheme-mode-hook  'turn-on-ctags-auto-update-mode)
(add-hook 'sh-mode-hook  'turn-on-ctags-auto-update-mode)
(setq tags-file-name "/tmp/TAGS")

(autoload 'ctags-update "ctags-update" "update TAGS using ctags" t)

(provide 'setup-etags-utilities)
