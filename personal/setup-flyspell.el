(autoload 'flyspell-mode "flyspell" "On-the-fly spelling checker." t)
(add-hook 'message-mode-hook 'turn-on-flyspell)
(add-hook 'org-mode-hook '(lambda () (flyspell-mode -1)))
(add-hook 'text-mode-hook 'turn-on-flyspell)
(add-hook 'git-commit-mode-hook 'turn-on-flyspell)
(add-hook 'git-commit-mode-hook (lambda () (toggle-save-place 0)))

;; (add-hook 'tcl-mode-hook 'flyspell-prog-mode)
;; (add-hook 'c-mode-common-hook 'flyspell-prog-mode)
;; (add-hook 'c++-mode-common-hook 'flyspell-prog-mode)
;; (add-hook 'emacs-lisp-mode-hook 'flyspell-prog-mode)
;; (add-hook 'c++-mode-hook 'turn-on-flyspell-prog-mode t)

(eval-after-load "flyspell"
  '(progn
     (define-key flyspell-mode-map (kbd "C-;") nil)
     (define-key flyspell-mode-map (kbd "C-.") nil)
     (global-set-key (kbd "M-;") 'evilnc-comment-or-uncomment-lines)
     (global-set-key (kbd "C-;") 'evilnc-comment-or-uncomment-to-the-line)))

(defun turn-on-flyspell ()
  "Force flyspell-mode on using a positive arg.  For use in hooks."
  (interactive)
  (flyspell-mode 1))

(require 'auto-dictionary)
(add-hook 'flyspell-mode-hook (lambda () (auto-dictionary-mode 1)))

(require 'flyspell-lazy)
(flyspell-lazy-mode 1)
(flyspell-mode 1)      ; or (flyspell-prog-mode)

(provide 'setup-flyspell)
