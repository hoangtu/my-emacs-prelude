(require 'linum-relative)
(linum-relative-toggle)
(setq linum-format " %3d")

(provide 'setup-linum)
