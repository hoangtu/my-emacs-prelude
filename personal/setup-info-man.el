(require 'woman)

(defun Info-man-completion (_caller _info string predicate action)
  "Add man entries to info completion."
  ;; prepare woman:
  (unless (and woman-expanded-directory-path woman-topic-all-completions)
    (setq woman-expanded-directory-path
		  (woman-expand-directory-path woman-manpath woman-path)
		  woman-topic-all-completions
		  (woman-topic-all-completions woman-expanded-directory-path)))
  ;; do completions:
  (cond
   ((null action) ;; try-completion
    ;; shortest wins
    (let ((_man (try-completion string woman-topic-all-completions)))
      (cond
       ((eq _info t)
		t)
       ((eq _man t)
		t)
       ((and (stringp _info) (stringp _man))
		(if (> (length _info) (length _man))
			_man
		  _info))
       ((stringp _info)
		_info)
       (t _man)
       )))
   ((eq action t) ;; all-completions
    (let ((_man (all-completions string woman-topic-all-completions)))
      (append _info _man)
      ))
   ((eq action 'lambda) ;; test-completion
    (try-completion string _caller))
   ((eq action 'metadata) ;; state of current completion
    '(metadata) ;; no specification
    )))

;; args: string predicate code
(defadvice Info-read-node-name-1 (around man activate)
  "Add man entries to info completion."
  (setq ad-return-value (apply 'Info-man-completion 'Info-read-node-name-1 ad-do-it (ad-get-args 0))))

;;
(defadvice Info-complete-menu-item (around man activate)
  "Add man entries to info completion."
  (setq ad-return-value (apply 'Info-man-completion 'Info-complete-menu-item ad-do-it (ad-get-args 0))))

(defadvice Info-goto-node (around man activate)
  "If no info node is found for string lookup and show man entry."
  (condition-case err
      ad-do-it
    (user-error
     (let ((err-str (car-safe (cdr err))))
       (if (and (stringp err-str)
				(string-match "No such node or anchor:" err-str))
		   (man (ad-get-arg 0))
		 (signal 'user-error err-str)
		 )))))

(defadvice Info-menu (around man activate)
  "If no info menu entry is found for string lookup and show man entry."
  (condition-case err
      ad-do-it
    (user-error
     (let ((err-str (car-safe (cdr err))))
       (if (and (stringp err-str)
				(string-match "No such item in menu" err-str))
		   (man (ad-get-arg 0))
		 (signal 'user-error err-str)
		 )))))

(defadvice Info-apropos-find-node (after man activate)
  "Add man appropos to info appropos."
  (let (item)
    (goto-char (point-max))
    (let ((inhibit-read-only t))
      (insert "\nMatches found by man-apropos\n\n")
      (let ((beg (point))
			(nodeinfo (assoc nodename Info-apropos-nodes)))
        (if nodeinfo
			(let ((search-string (nth 1 nodeinfo)))
			  (call-process "apropos" nil t t search-string)
			  (goto-char beg)
			  (while (re-search-forward "^\\(\\(?:[[:alnum:]]\\|\\s_\\)+\\)\\(?:[[:blank:]]+\\[\\]\\)?\\([[:blank:]]+([[:alnum:]]+)\\)[[:blank:]]+-[[:blank:]]+\\(.*\\)$" nil t)
				(replace-match (replace-regexp-in-string "\\\\" "\\\\\\\\" (format "* %-38s.%s"
																				   (format "%s:" (match-string 1))
																				   (concat (match-string 1) (match-string 2))
																				   (match-string 3))))))
          (man nodename)
          )))))
