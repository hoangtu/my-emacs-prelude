;;rainbow
(require 'rainbow-delimiters)


;; Add hooks for modes where you want it enabled, for example:
(add-hook 'clojure-mode-hook 'rainbow-delimiters-mode)
(add-hook 'slime-mode-hook 'rainbow-delimiters-mode)
;; (add-hook 'c-mode-common-hook 'rainbow-delimiters-mode)
(add-hook 'sh-mode 'rainbow-delimiters-mode)
(add-hook 'sml-mode 'rainbow-delimiters-mode)
(add-hook 'scheme-mode-hook 'rainbow-delimiters-mode)
;; (global-rainbow-delimiters-mode)


(provide 'setup-rainbow-delimiters)
