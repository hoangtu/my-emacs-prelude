(defadvice Info-read-node-name-1 (after man activate)
  (when (and string (null (string= string ""))
			 (null (ad-get-arg 2))
			 (or (null ad-return-value) (string= ad-return-value "")))
    (setq ad-return-value t)))

(defadvice Info-complete-menu-item (after man activate)
  (when (and string (null (string= string ""))
			 (null (ad-get-arg 2))
			 (or (null ad-return-value) (string= ad-return-value "")))
    (setq ad-return-value t)))

(defadvice Info-goto-node (around man activate)
  (condition-case err
      ad-do-it
    (user-error
     (let ((err-str (car-safe (cdr err))))
       (if (and (stringp err-str)
				(string-match "No such node or anchor:" err-str))
		   (man (ad-get-arg 0))
		 (signal 'user-error err-str)
		 )))))

(defadvice Info-menu (around man activate)
  (condition-case err
      ad-do-it
    (user-error
     (let ((err-str (car-safe (cdr err))))
       (if (and (stringp err-str)
				(string-match "No such item in menu" err-str))
		   (man (ad-get-arg 0))
		 (signal 'user-error err-str)
		 )))))

(defadvice Info-apropos-find-node (after man activate)
  (let (item)
    (goto-char (point-max))
    (let ((inhibit-read-only t))
      (insert "\nMatches found by man-apropos\n\n")
      (let ((beg (point))
			(nodeinfo (assoc nodename Info-apropos-nodes)))
        (if nodeinfo
			(let ((search-string (nth 1 nodeinfo)))
			  (call-process "apropos" nil t t search-string)
			  (goto-char beg)
			  (while (re-search-forward "^\\(\\(?:[[:alnum:]]\\|\\s_\\)+[[:blank:]]+([[:alnum:]]+)\\)[[:blank:]]+-[[:blank:]]+\\(.*\\)$" nil t)
				(replace-match (replace-regexp-in-string "\\\\" "\\\\\\\\" (format "* %-38s.%s"
																				   (format "%s:" (match-string 1))
																				   (match-string 1)
																				   (match-string 2))))))
          (man nodename)
          )))))
