(require 'synonyms)
(setq synonyms-file (concat *emacs-dir* "/mthesaur.txt"))
(setq synonyms-cache-file (concat *emacs-dir* "/savefile/mtehsaur.cache"))

(provide 'setup-synonyms)
