(require 'helm)
(require 'helm-config)
(require 'helm-eshell)
(require 'helm-files)
(require 'helm-adaptive)
(helm-adaptive-mode 1)
(require 'wgrep-helm)

(define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action)
(define-key helm-map (kbd "C-i") 'helm-execute-persistent-action)
(define-key helm-map (kbd "C-z")  'helm-select-action)

(eval-after-load 'helm-grep
  '(progn
     (define-key helm-grep-mode-map (kbd "<return>")  'helm-grep-mode-jump-other-window)
     (define-key helm-grep-mode-map (kbd "n")  'helm-grep-mode-jump-other-window-forward)
     (define-key helm-grep-mode-map (kbd "p")  'helm-grep-mode-jump-other-window-backward)))

(setq helm-google-suggest-use-curl-p t
                                        ;helm-kill-ring-threshold 1
      ;; helm-raise-command "wmctrl -xa %s"
      helm-scroll-amount 4
      helm-quick-update t
      helm-idle-delay 0.01
      helm-input-idle-delay 0.01
                                        ;helm-completion-window-scroll-margin 0
                                        ;helm-display-source-at-screen-top nil
      helm-ff-search-library-in-sexp t
                                        ;helm-kill-ring-max-lines-number 5
      ;; helm-default-external-file-browser "thunar"
      ;; helm-pdfgrep-default-read-command "evince --page-label=%p '%f'"
                                        ;helm-ff-transformer-show-only-basename t
      ;; helm-ff-auto-update-initial-value t
      ;; helm-grep-default-command "ack-grep -Hn --smart-case --no-group --no-color %e %p %f"
      ;; helm-grep-default-recurse-command "ack-grep -H --smart-case --no-group --no-color %e %p %f"
      ;; helm-reuse-last-window-split-state t
                                        ;helm-split-window-default-side 'other
                                        ;helm-split-window-in-side-p nil
      ;; helm-always-two-windows t
                                        ;helm-persistent-action-use-special-display t
      helm-buffers-favorite-modes (append helm-buffers-favorite-modes
                                          '(picture-mode artist-mode))
      helm-ls-git-status-command 'magit-status
                                        ;helm-never-delay-on-input nil
      helm-candidate-number-limit 200
      helm-M-x-requires-pattern 0
      ;; helm-dabbrev-cycle-thresold 5
      ;; helm-surfraw-duckduckgo-url "https://duckduckgo.com/?q=%s&ke=-1&kf=fw&kl=fr-fr&kr=b&k1=-1&k4=-1"
      helm-boring-file-regexp-list '("\\.git$" "\\.hg$" "\\.svn$" "\\.CVS$" "\\._darcs$" "\\.la$" "\\.o$" "\\.i$")
                                        ;helm-mode-handle-completion-in-region t
                                        ;helm-moccur-always-search-in-current t
                                        ;helm-tramp-verbose 6
                                        ;helm-ff-file-name-history-use-recentf t
                                        ;helm-follow-mode-persistent t
      helm-move-to-line-cycle-in-source t
      ido-use-virtual-buffers t ; Needed in helm-buffers-list
      helm-tramp-verbose 6
      helm-buffers-fuzzy-matching t
      )

;; (define-key helm-map (kbd "C-x 2") 'helm-select-2nd-action)
;; (define-key helm-map (kbd "C-x 3") 'helm-select-3rd-action)
;; (define-key helm-map (kbd "C-x 4") 'helm-select-4rd-action)

;; (global-set-key (kbd "M-x") 'helm-M-x)
;; (global-set-key (kbd "C-x b") 'helm-mini)

;; (global-set-key (kbd "C-c f") 'helm-recentf)

;; (add-hook 'eshell-mode-hook
;;           #'(lambda ()
;;               (define-key eshell-mode-map (kbd "C-c C-l")  'helm-eshell-history)))

;; (setq helm-display-function
;;       (lambda (buf)
;;         (split-window-horizontally)
;;         (other-window 1)
;;         (switch-to-buffer buf)))

;;; Debugging
;;
;;
(defun helm-debug-toggle ()
  (interactive)
  (setq helm-debug (not helm-debug))
  (message "Helm Debug is now %s"
           (if helm-debug "Enabled" "Disabled")))

(defun helm-ff-candidates-lisp-p (candidate)
  (cl-loop for cand in (helm-marked-candidates)
           always (string-match "\.el$" cand)))

;; Add magit to `helm-source-ls-git'
;; (helm-add-action-to-source
;;  "Magit status"
;;  #'(lambda (_candidate)
;;      (with-helm-buffer (magit-status helm-default-directory)))
;;  helm-source-ls-git 1)


;;; Save current position to mark ring
;;
;; (add-hook 'helm-goto-line-before-hook 'helm-save-current-pos-to-mark-ring)

(require 'helm-ack)
(setq helm-c-ack-auto-set-filetype nil)
(setq helm-c-ack-insert-at-point'symbol)

(require 'helm-ag)
(setq helm-ag-insert-at-point 'symbol)
(setq helm-ag-source-type 'file-line)

(require 'helm-flycheck) ;; Not necessary if using ELPA package

(require 'helm-swoop)

(require 'helm-descbinds)
(helm-descbinds-mode)

(require 'helm-orgcard)
;;; Enable helm-gtags-mode
;; (add-hook 'c-mode-hook 'helm-gtags-mode)
;; (add-hook 'c++-mode-hook 'helm-gtags-mode)
;; (add-hook 'asm-mode-hook 'helm-gtags-mode)

;; customize
;; (custom-set-variables
;;  '(helm-gtags-path-style 'relative)
;;  '(helm-gtags-ignore-case t)
;;  '(helm-gtags-auto-update t))

;; key bindings
;; (eval-after-load "helm-gtags"
;;   '(progn
;;      (define-key helm-gtags-mode-map (kbd "M-t") 'helm-gtags-find-tag)
;;      (define-key helm-gtags-mode-map (kbd "M-r") 'helm-gtags-find-rtag)
;;      (define-key helm-gtags-mode-map (kbd "M-s") 'helm-gtags-find-symbol)
;;      (define-key helm-gtags-mode-map (kbd "M-g M-p") 'helm-gtags-parse-file)
;;      (define-key helm-gtags-mode-map (kbd "C-c <") 'helm-gtags-previous-history)
;;      (define-key helm-gtags-mode-map (kbd "C-c >") 'helm-gtags-next-history)
;;      (define-key helm-gtags-mode-map (kbd "M-,") 'helm-gtags-pop-stack)))

;; Save buffer when helm-multi-swoop-edit complete
(setq helm-multi-swoop-edit-save t)

;; If this value is t, split window inside the current window
(setq helm-swoop-split-with-multiple-windows t)

;; Split direcion. 'split-window-vertically or 'split-window-horizontally
(setq helm-swoop-split-direction 'split-window-vertically)

;; (helm-occur-init-source)

;; (helm-attrset 'follow 1 helm-source-occur)
;; (helm-attrset 'follow 1 helm-source-moccur)

(provide 'setup-helm)
