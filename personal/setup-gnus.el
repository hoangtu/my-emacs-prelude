(setq user-mail-address "solidius4747@gmail.com")
(setq user-full-name "Tu, Do")


;; (setq gnus-select-method
;;       '(nnimap "gmail"
;;                (nnimap-address "imap.gmail.com")
;;                (nnimap-server-port 993)
;;                (nnimap-stream ssl)))

;; (setq message-send-mail-function 'smtpmail-send-it
;;       smtpmail-starttls-credentials '(("smtp.gmail.com" 587 nil nil))
;;       smtpmail-auth-credentials '(("smtp.gmail.com" 587
;;                                    "solidius4747@gmail.com" nil))
;;       smtpmail-default-smtp-server "smtp.gmail.com"
;;       smtpmail-smtp-server "smtp.gmail.com"
;;       smtpmail-smtp-service 587
;;       gnus-ignored-newsgroups "^to\\.\\|^[0-9. ]+\\( \\|$\\)\\|^[\"]\"[#'()]")


(setq gnus-select-method '(nnml ""))
;; (add-to-list 'gnus-secondary-select-methods '(nntp "localhost"))
;; (add-to-list 'gnus-secondary-select-methods '(nntp "mail.dektech.com.au")) ;

;; (add-to-list 'gnus-secondary-select-methods '(nnml ""))

(setq mail-sources '((pop :server "mail.dektech.com.au"
                          :port 995
                          :user "tu.h.do"
                          :stream ssl)))


;; (setq nnmail-crosspost nil
;;       nnmail-split-methods 'nnmail-split-fancy
;;       nnmail-split-fancy
;;       `(| (any "bao.*@dektech.com.au" "mail.Bao")
;;           (any "brendan.*@dektech.com.au" "mail.Brendan")
;;           "mail.spam"))

(setq nnmail-split-methods
      (list
       ;; Mailing lists that don't list my name in the headers
       (list "mail.Bredan" (concat "^From:.*" (regexp-opt
                                               '("Brendan Gannon"
                                                 "brendan.*@dektech.com.au") 1)))
       (list "mail.Bao" (concat "^From:.*" (regexp-opt
                                            '("Bao Nguyen"
                                              "bao.*@dektech.com.au") 1)))
       (list "mail.Daniel" (concat "^From:.*" (regexp-opt
                                               '("Daniel Tedesco"
                                                 "daniel.*@dektech.com.au") 1)))
       (list "mail.Loan" (concat "^From:.*" (regexp-opt
                                             '("Loan Phan"
                                               "loan.*@dektech.com.au") 1)))
       ;; Mails from myself and to myself
       ;; The rest is spam
       '("mail.Inbox" "")
       ))
                                        ; Tell gnus which method to use for archives (nnfolder)
(setq gnus-message-archive-method 
      '(nnfolder "archive"
                 (nnfolder-directory   "~/.mails/")
                 (nnfolder-active-file "~/.mails/active")
                 (nnfolder-get-new-mail nil)
                 (nnfolder-inhibit-expiry t)))
                                        ; Tell gnus into which group to store messages
(setq gnus-message-archive-group
      '((if (message-news-p)
            "misc-news"
          (concat "mail." (format-time-string "%Y-%m" (current-time))))))

(add-hook 'gnus-after-getting-new-news-hook 'gnus-notifications)

(defun get-bookmarks-in-new-frame ()
  (interactive)
  (bookmark-bmenu-list)
  (switch-to-buffer-other-window "*Bookmark List*"))

(add-hook 'gnus-after-getting-new-news-hook 'gnus-notifications)

(provide 'setup-gnus)
