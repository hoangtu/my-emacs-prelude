(require 'pager)

(global-set-key "\C-v"         'pager-page-down)
(global-set-key [next]          'pager-page-down)
(global-set-key "\ev"         'pager-page-up)
(global-set-key [prior]         'pager-page-up)

(provide 'setup-pager)