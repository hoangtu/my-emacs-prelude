# Sample .bashrc for SuSE Linux
# Copyright (c) SuSE GmbH Nuernberg

# There are 3 different types of shells in bash: the login shell, normal shell
# and interactive shell. Login shells read ~/.profile and interactive shells
# read ~/.bashrc; in our setup, /etc/profile sources ~/.bashrc - thus all
# settings made here will also take effect in a login shell.
#
# NOTE: It is recommended to make language settings in ~/.profile rather than
# here, since multilingual X sessions would not work properly if LANG is over-
# ridden in every subshell.

# Some applications read the EDITOR variable to determine your favourite text
# editor. So uncomment the line below and enter the editor of your choice :-)
#export EDITOR=/usr/bin/vim
#export EDITOR=/usr/bin/mcedit

# For some news readers it makes sense to specify the NEWSSERVER variable here
#export NEWSSERVER=your.news.server

# If you want to use a Palm device with Linux, uncomment the two lines below.
# For some (older) Palm Pilots, you might need to set a lower baud rate
# e.g. 57600 or 38400; lowest is 9600 (very slow!)
#
#export PILOTPORT=/dev/pilot
#export PILOTRATE=115200



test -s ~/.alias && . ~/.alias || true
set -o noclobber
alias df='df -h'
#alias rm='rm -i'
alias lt='ls -ltrh | tail'
alias h='history | tail'
alias ch='chmod 755'
alias ls='ls --color=auto'
PS1='\e[0;31m\u@\h\e[m \e[0;34m\w\e[m \e[0;35m\d \A\e[m\n$ '
function switch()
{
	  local tmp=$$switch
	  mv "$1" $tmp
	  mv "$2" "$1"
	  mv $tmp "$2"
}

export lat2=~/enux/test/lat2
export lat2_is=~/enux/test/lat2_is
export lat_log=~/latlog

alias emacs='~/programs/emacs-24.3/bin/emacs -nw'
alias emacsclient='~/programs/emacs-24.3/bin/emacsclient'
alias em='emacsclient -t -c'
export LD_LIBRARY_PATH=/home/xtuudoo/usr/lib
PATH=$PATH:/home/xtuudoo/programs/fasd/bin
emacs_start=`ps -A | grep emacs`
if [ "$emacs_start" == '' ]; then
    emacs --daemon
fi
VISUAL="emacsclient -c"
EDITOR="emacsclient -c"
TERM=xterm-256color
eval "$(fasd --init auto)"
