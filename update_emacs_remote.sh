#!/bin/bash
remotes=( jogvan bojohnny tree leaf twig )

function update_emacs_local {
    cd .emacs.d
    git stash save --keep-index
    git pull
    kill $(ps -u xtuudoo x | grep 'emacs.*daemon' | awk '{print $1}')
    emacs --daemon
}

function update_emacs_remote {
    for i in "${remotes[@]}"; do
        echo "Updating Emacs on $i..."
        ssh $1@$i "cd .emacs.d ; git stash save --keep-index; git pull"

        # if [[ "$i" != "twig" && "$i" != "tree" ]]; then
        #     echo "Commenting scroll-bar-mode and nyan-mode on $i"
        #     ssh $1@$i "cd .emacs.d; cat misc-settings.el | sed 's/^(scroll-bar-mode -1)$/;;(scroll-bar-modeoll-bar-mode -1)/' | sed 's/^(nyan-mode)$/;;(nyan-mode)/' > tmp ; mv tmp misc-settings.el"
        # fi

        echo ""
    done
}

function start_emacs_daemon {
    for i in "${remotes[@]}"; do
        echo "Kill old Emacs Daemon on $i..."
        ssh $1@$i "kill \$(ps -u xtuudoo x | grep 'emacs.*daemon' | awk '{print \$1}' )"
        echo "Restart Emacs Daemon on $i..."
        ssh $1@$i "PATH=$PATH:/home/xtuudoo/programs/ctags-5.8/bin;/home/$1/programs/emacs-24.3/bin/emacs -nw --daemon"
        echo ""
    done
}

update_emacs_local
update_emacs_remote $1
start_emacs_daemon $1
