;; General

(eval-after-load 'prelude-mode
  '(progn
     (define-key prelude-mode-map (kbd "M-o")  nil)
     (define-key prelude-mode-map (kbd "C-x m")  nil)
     (define-key prelude-mode-map (kbd "C-c C-t")  nil)
     (define-key prelude-mode-map (kbd "C-c n")  nil)
     (define-key prelude-mode-map (kbd "C-c s")  nil)
     (define-key prelude-mode-map (kbd "C-c j")  nil)
     (define-key prelude-mode-map (kbd "C-c k")  nil)
     (define-key prelude-mode-map (kbd "C-c h")  nil)
     (define-key prelude-mode-map (kbd "C-c g")  nil)
     ;; (define-key prelude-mode-map (kbd "C-c o")  nil)
     (define-key prelude-mode-map (kbd "C-c f")  nil)
     (define-key prelude-mode-map (kbd "C-c S")  nil)
     (define-key prelude-mode-map (kbd "C-c y")  nil) ;; reserve
     (define-key prelude-mode-map (kbd "C-c i")  nil)
     (define-key prelude-mode-map (kbd "C-c d")  nil)
     (define-key prelude-mode-map (kbd "C-c D")  nil)
     (define-key prelude-mode-map (kbd "C-c +") nil)
     (define-key prelude-mode-map (kbd "C-c -") nil)
     (define-key prelude-mode-map (kbd "C-c C-s") nil)))

(global-set-key (kbd "C-t") 'linum-relative-toggle)
(global-set-key (kbd "M-z") 'zop-to-char)
(global-set-key (kbd "C-h TAB") 'prelude-cleanup-buffer)
(global-set-key (kbd "C-h C-0") 'edebug-defun)
(key-chord-define-global "uu" nil)
(key-chord-define-global "xx" nil)
(key-chord-define-global "jj" 'ace-jump-word-mode)
(key-chord-define-global "jk" 'ace-jump-char-mode)
;; (key-chord-define-global "yy" 'helm-show-kill-ring)
;; (global-set-key "\C-c\C-w" 'just-one-space)

(key-chord-define-global ",," 'god-local-mode)

;; (global-set-key (kbd "C-h C-x") (lambda () (interactive) (save-buffers-kill-emacs)))
;; (global-unset-key (kbd "C-x C-c"))
(global-unset-key (kbd "C-h s"))
(global-unset-key (kbd "C-h g"))
(global-unset-key (kbd "C-h i"))
(global-unset-key (kbd "C-h C-h"))
(global-set-key (kbd "C-h o") 'helm-orgcard)
(global-set-key (kbd "C-h g") 'helm-google-suggest)
(global-set-key (kbd "C-h h") 'helm-projectile-all)
(global-set-key (kbd "C-h i i") 'helm-info-at-point)
(global-set-key (kbd "C-h i e") 'helm-info-emacs)
(global-set-key (kbd "C-h i l") 'helm-info-elisp)
(global-set-key (kbd "C-h n") 'howdoi-query)
(global-set-key (kbd "C-h C-l") 'helm-locate-library)
(global-set-key (kbd "C-h C-s") 'helm-google)
(global-set-key (kbd "C-h C-m") 'discover-my-major)
(global-set-key (kbd "C-h t") 'google-translate-at-point)
(global-set-key (kbd "C-h C-w") 'helm-wikipedia-suggest)
(global-set-key (kbd "C-h C-b") 'eval-buffer)
(global-set-key (kbd "C-h C-e") 'toggle-debug-on-error)
(global-set-key (kbd "C-h C-j") 'ipretty-last-sexp)
(global-set-key (kbd "C-h C-k") 'ipretty-last-sexp-other-buffer)
(global-set-key (kbd "C-h SPC") 'helm-all-mark-rings)
(global-set-key (kbd "C-h <return>") 'eww)
(global-set-key (kbd "C-h M-.") 'my-eww-browse-dwim)
(global-set-key (kbd "C-c C-b") 'browse-url-at-point)
(global-set-key (kbd "C-c q") 'auto-fill-mode)

(global-unset-key (kbd "C-\\")) ;; unbinds key for future use
;; (global-set-key (kbd "M-/") 'undo-tree-redo)

(key-chord-define-global "qw" 'kill-next-pane)
(key-chord-define-global "fg" 'beginning-of-buffer)
(key-chord-define-global "bv" 'end-of-buffer)

;; key bindings for text edit
(global-set-key (kbd "M-m") 'er/expand-region)

;; align-cols
(global-set-key (kbd "C-M-/") 'align-cols)

;; ahs
;; (global-auto-highlight-symbol-mode)
;; (define-key auto-highlight-symbol-mode-map (kbd "M-p") 'ahs-backward)
;; (define-key auto-highlight-symbol-mode-map (kbd "M-n") 'ahs-forward)
;; (define-key auto-highlight-symbol-mode-map (kbd "M-P") 'ahs-backward-definition)
;; (define-key auto-highlight-symbol-mode-map (kbd "M-S") 'ahs-forward-definition)
;; (define-key auto-highlight-symbol-mode-map (kbd "C-c x") 'ahs-back-to-start)
;; (define-key auto-highlight-symbol-mode-map (kbd "C-x C-'") 'ahs-change-range)
;; (define-key auto-highlight-symbol-mode-map (kbd "C-x C-a") 'ahs-edit-mode)

;; buf-move
(define-key ctl-x-map (kbd "<up>")     'buf-move-up)
(define-key ctl-x-map (kbd "<down>")     'buf-move-down)
(define-key ctl-x-map (kbd "<left>")     'buf-move-left)
(define-key ctl-x-map (kbd "<right>")     'buf-move-right)

;; wind-move
;; (global-set-key (kbd "C-c C-<up>")     'windmove-up)
;; (global-set-key (kbd "C-c C-<down>")   'windmove-down)
;; (global-set-key (kbd "C-c C-<left>")   'windmove-left)
;; (global-set-key (kbd "C-c C-<right>")  'windmove-right)

(global-set-key (kbd "C-c d") 'prelude-delete-file-and-buffer)


;; easy-kill
(global-set-key (kbd "M-w") 'kill-ring-save)
(global-set-key [remap kill-ring-save] 'easy-kill)
(global-set-key [remap mark-sexp] 'easy-mark)
;; file and buffer kill
;; (global-set-key (kbd "C-x d") 'prelude-delete-file-and-buffer)

;; linum-relative
(global-set-key (kbd "C-c w") 'whitespace-mode)

;; function-args
(define-key c-mode-map  [(tab)] 'moo-complete)
(define-key c-mode-map  [(control tab)] 'company-complete)
(define-key c++-mode-map  [(tab)] 'moo-complete)
(define-key c++-mode-map  [(control tab)] 'company-complete)

;; company key bindings
(define-key c-mode-map (kbd "M-l") 'company-show-location)
(define-key c-mode-map (kbd "M-s") 'company-show-doc-buffer)
(define-key c++-mode-map (kbd "M-l") 'company-show-location)
(define-key c++-mode-map (kbd "M-k") 'company-show-doc-buffer)

;; cedit
(defun cedit-mode-hook ()
  ;; (local-set-key (kbd "C-f") 'cedit-forward-char)
  ;; (local-set-key (kbd "C-b") 'cedit-backward-char)
  (local-set-key (kbd "M-a") 'cedit-beginning-of-statement)
  (local-set-key (kbd "M-e") 'cedit-end-of-statement)
  (local-set-key (kbd "C-M-d") 'cedit-down-block)
  (local-set-key (kbd "C-M-u") 'cedit-up-block-forward)
  (local-set-key (kbd "C-<right>") 'cedit-slurp)
  (local-set-key (kbd "C-<left>") 'cedit-barf)
  ;; (local-set-key (kbd "M-<up>") 'cedit-raise)
  )

(add-hook 'c-mode-common-hook 'cedit-mode-hook)

;;dired
(global-set-key (kbd  "C-x d") 'diredp-dired-files)
(global-set-key (kbd "C-x C-d") 'diredp-dired-files-other-window)

;; symbol navigation
(global-set-key [(meta shift h)] 'highlight-symbol-at-point)
;; (global-set-key (kbd "M-n") 'highlight-symbol-next)
;; (global-set-key (kbd "M-p") 'highlight-symbol-prev)

;; (define-key dired-mode-map (kbd "C-s") 'dired-isearch-forward)
;; (define-key dired-mode-map (kbd "C-r") 'dired-isearch-backward)
;; (define-key dired-mode-map (kbd "ESC C-s") 'dired-isearch-forward-regexp)
;; (define-key dired-mode-map (kbd "ESC C-r") 'dired-isearch-backward-regexp)

;; doremi
;; (defalias 'doremi-prefix (make-sparse-keymap))
;; (defvar doremi-map (symbol-function 'doremi-prefix)
;;   "Keymap for Do Re Mi commands.")

;; ;; doremi-commands
;; (define-key global-map "\C-xt" 'doremi-prefix)
;; (define-key doremi-map "b" 'doremi-buffers+)
;; (define-key doremi-map "g" 'doremi-global-marks+)
;; (define-key doremi-map "m" 'doremi-marks+)
;; (define-key doremi-map "r" 'doremi-bookmarks+) ; reading books?
;; (define-key doremi-map "s" 'doremi-custom-themes+) ; custom schemes
;; (define-key doremi-map "w" 'doremi-window-height+)

;; ;;doremi frame
;; (define-key doremi-map "a" 'doremi-all-faces-fg+)    ; "All"
;; (define-key doremi-map "c" 'doremi-bg+)              ; "Color"
;; (define-key doremi-map "f" 'doremi-face-fg+)         ; Face"
;; (define-key doremi-map "h" 'doremi-frame-height+)
;; (define-key doremi-map "t" 'doremi-font+)            ; "Typeface"
;; (define-key doremi-map "u" 'doremi-frame-configs+)   ; "Undo"
;; (define-key doremi-map "x" 'doremi-frame-horizontally+)
;; (define-key doremi-map "y" 'doremi-frame-vertically+)
;; (define-key doremi-map "z" 'doremi-font-size+)      ; "Zoom

;; etags & gtags
;; (global-set-key "\C-c\C-t" 'ctags-update)
(global-set-key "\M-." 'helm-gtags-select)
(global-set-key (kbd "C-c g s") 'ggtags-find-other-symbol)
(global-set-key (kbd "C-c g h") 'ggtags-view-tag-history)
(global-set-key (kbd "C-c g r") 'ggtags-find-reference)
(global-set-key (kbd "C-c g f") 'ggtags-find-file)
(global-set-key (kbd "C-c g t") 'helm-gtags-select)
(global-set-key (kbd "C-c g c") 'ggtags-create-tags)
(global-set-key (kbd "C-c g u") 'ggtags-update-tags)
(eval-after-load "ggtags"
  '(progn
     ;; (define-key ggtags-mode-map (kbd "M-.") 'ggtags-find-definition)
     (define-key ggtags-navigation-mode-map (kbd "C-c s") 'ggtags-navigation-isearch-forward)))

;; policy-switch
(global-set-key (kbd "C-c x <right>") 'policy-switch-policy-next)
(global-set-key (kbd "C-c x a") 'policy-switch-policy-add)
(global-set-key (kbd "C-c x g") 'policy-switch-policy-goto)
(global-set-key (kbd "C-c x <left>") 'policy-switch-policy-prev)
(global-set-key (kbd "C-c x r") 'policy-switch-policy-remove)
(global-set-key (kbd "C-c x n") 'policy-switch-config-next)
(global-set-key (kbd "C-c x p") 'policy-switch-config-prev)
(global-set-key (kbd "C-c x s") 'policy-switch-config-goto)
(global-set-key (kbd "C-c x w") 'policy-switch-config-add)
(global-set-key (kbd "C-c x d") 'policy-switch-config-remove)
(global-set-key (kbd "C-c x u") 'policy-switch-config-restore)
(global-set-key (kbd "C-c x m") 'policy-switch-toggle-mode-line)

;; eww
(eval-after-load "eww"
  '(progn (define-key eww-mode-map "f" 'eww-lnum-follow)
          (define-key eww-mode-map "F" 'eww-lnum-universal)))

;; fuzzy-format
;; (global-set-key (kbd "C-x C-t") 'tabify)
;; (global-set-key (kbd "C-x C-u") 'untabify)
;; (global-set-key (kbd "C-x C-u") 'fuzzy-format-set-spaces-mode)
;; (global-set-key (kbd "C-x C-t") 'fuzzy-format-set-tabs-mode)

;; fasd
(global-set-key (kbd "C-c z") 'fasd-find-file)

;; fold-this
(global-set-key (kbd "C-c f a") 'fold-this-all)
(global-set-key (kbd "C-c f f") 'fold-this)
(global-set-key (kbd "C-c f u") 'fold-this-unfold-all)
(global-set-key (kbd "M-g M-f") 'fci-mode)

;; ido
;; (global-set-key (kbd "C-x C-d") 'ido-dired)

(defun sd/ido-define-keys ()
  (define-key ido-completion-map (kbd "C-n") 'ido-next-match)
  ;; (define-key ido-completion-map (kbd "<down>") 'ido-next-match)
  (define-key ido-completion-map (kbd "C-p") 'ido-prev-match)
  ;; (define-key ido-completion-map (kbd "<up>") 'ido-prev-match)
  ) ;; C-n/p is more intuitive in vertical layout

(defun my/setup-ido ()
  ;; Go straight home
  ;; (define-key ido-file-completion-map
  ;;   (kbd "~")
  ;;   (lambda ()
  ;;     (interactive)
  ;;     (cond
  ;;      ((looking-back "~/") (insert "projects/"))
  ;;      ((looking-back "/") (insert "~/"))
  ;;      (:else (call-interactively 'self-insert-command)))))

  ;; Use C-w to go back up a dir to better match normal usage of C-w
  ;; - insert current file name with C-x C-w instead.
  (define-key ido-file-completion-map (kbd "C-w") 'ido-delete-backward-updir)
  (define-key ido-file-completion-map (kbd "C-x C-w") 'ido-copy-current-file-name)

  (define-key ido-file-dir-completion-map (kbd "C-w") 'ido-delete-backward-updir)
  (define-key ido-file-dir-completion-map (kbd "C-x C-w") 'ido-copy-current-file-name))

(add-hook 'ido-setup-hook 'my/setup-ido)
(add-hook 'ido-setup-hook
          (lambda()
            (define-key ido-completion-map (kbd "C-M-p") (lookup-key ido-completion-map (kbd "C-p")))
            (define-key ido-completion-map (kbd "C-M-n") (lookup-key ido-completion-map (kbd "C-n"))) ; currently, this makes nothing. Maybe they'll make C-n key lately.
            (define-key ido-completion-map (kbd "C-p") 'ido-preview-backward)
            (define-key ido-completion-map (kbd "C-n") 'ido-preview-forward)))

;; xcscope
;; (define-key global-map [(control f3)]  'cscope-set-initial-directory)
;; (define-key global-map [(control f4)]  'cscope-unset-initial-directory)
;; (define-key global-map [(control f5)]  'cscope-find-this-symbol)
;; (define-key global-map [(control f6)]  'cscope-find-global-definition)
;; (define-key global-map [(control f7)]
;;   'cscope-find-global-definition-no-prompting)
;; (define-key global-map [(control f8)]  'cscope-pop-mark)
;; (define-key global-map [(control f9)]  'cscope-next-symbol)
;; (define-key global-map [(control f10)] 'cscope-next-file)
;; (define-key global-map [(control f11)] 'cscope-prev-symbol)
;; (define-key global-map [(control f12)] 'cscope-prev-file)
;; (define-key global-map [(meta f9)]  'cscope-display-buffer)
;; (define-key global-map [(meta f10)] 'cscope-display-buffer-toggle)

;; iasm
(global-set-key (kbd "C-c i d") 'iasm-disasm)
(global-set-key (kbd "C-c i l") 'iasm-ldd)

(add-hook 'c-mode-common-hook
          (lambda ()
            (local-set-key (kbd "C-c c C-b") 'iasm-goto-disasm-buffer)
            (local-set-key (kbd "C-c c C-k") 'iasm-disasm-link-buffer)))

;; compile
(global-set-key (kbd "<f6>") 'compile)
(global-set-key (kbd "C-<f6>") 'recompile)
(define-key c-mode-map (kbd "S-<f6>") 'gdb)
(define-key c++-mode-map (kbd "S-<f6>") 'gdb)

;; isearch
(define-key minibuffer-local-map (kbd "M-p") 'helm-minibuffer-history)
(define-key minibuffer-local-map (kbd "M-n") 'helm-minibuffer-history)

;; operate-on-number
(smartrep-define-key global-map "C-c ."
  '(("+" . apply-operation-to-number-at-point)
    ("-" . apply-operation-to-number-at-point)
    ("*" . apply-operation-to-number-at-point)
    ("/" . apply-operation-to-number-at-point)
    ("\\" . apply-operation-to-number-at-point)
    ("^" . apply-operation-to-number-at-point)
    ("<" . apply-operation-to-number-at-point)
    (">" . apply-operation-to-number-at-point)
    ("#" . apply-operation-to-number-at-point)
    ("%" . apply-operation-to-number-at-point)
    ("'" . operate-on-number-at-point)))

;; popwin
;; (global-set-key (kbd "C-x C-p") popwin:keymap)

;; dedicated
(global-set-key (kbd "C-c y") 'dedicated-mode)


;; evilnc
(global-set-key (kbd "M-;") 'evilnc-comment-or-uncomment-lines)
(global-set-key (kbd "C-M-;") 'evilnc-comment-or-uncomment-to-the-line)
;; (define-key paredit-mode-map (kbd "M-;") 'evilnc-comment-or-uncomment-lines)
;; (define-key paredit-mode-map (kbd "C-M-;") 'evilnc-comment-or-uncomment-to-the-line)

(global-set-key (kbd "M-c") 'prelude-duplicate-current-line-or-region)

;; occur-x
(define-key occur-mode-map "o" 'occur-mode-goto-occurrence-other-window)
(define-key occur-mode-map "\C-o" 'occur-mode-display-occurrence)

(global-set-key (kbd "C-x C-r") 'sudo-edit)

(global-set-key (kbd "C-c C-q") 'view-mode)

(global-set-key (kbd "C-\\") 'my-ido-hippie-expand)
(global-set-key (kbd "C-c \\") 'my-ido-hippie-expand-filename)
(global-set-key (kbd "M-o") 'prelude-smart-open-line-above)
(global-set-key (kbd "C-o") 'prelude-smart-open-line)
(global-set-key (kbd "C-x k") 'kill-default-buffer)
;; (global-set-key (kbd "C-c <SPC>") 'ace-jump-char-mode)
;; (global-set-key (kbd "C-c C-s")   'isearch-forward-regexp)
;; (global-set-key (kbd "C-c C-r")   'isearch-backward-regexp)

;; (global-set-key (kbd "C-M-s") 'isearch-forward)
;; (global-set-key (kbd "C-M-r") 'isearch-backward)

(global-set-key (kbd "C-x 4 C-f") 'smart-window-file-split)

;; (global-set-key (kbd "M-c") 'prelude-duplicate-current-line-or-region)
(global-unset-key (kbd "C-x q"))

;; shell-pop
(global-set-key (kbd "C-c s") 'shell-pop)

;; quickrun
(global-set-key (kbd "C-x q r") 'quickrun)
(global-set-key (kbd "C-x q b") 'quickrun-region)
(global-set-key (kbd "C-x q a") 'quickrun-with-arg)
(global-set-key (kbd "C-x q s") 'quickrun-shell)
(global-set-key (kbd "C-x q c") 'quickrun-compile-only)
(global-set-key (kbd "C-x q o") 'quickrun-replace-region)

;; magit
(global-unset-key (kbd "C-x g"))
(global-set-key (kbd "C-x g h") 'magit-log)
(global-set-key (kbd "C-x g f") 'magit-file-log)
(global-set-key (kbd "C-x g b") 'magit-blame-mode)
(global-set-key (kbd "C-x g m") 'magit-branch-manager)
(global-set-key (kbd "C-x g c") 'magit-branch)
(global-set-key (kbd "C-x g s") 'magit-status)
(global-set-key (kbd "C-x g g") 'helm-git-grep)
(global-set-key (kbd "C-x g l") 'helm-ls-git-ls)
(global-set-key (kbd "C-x g r") 'magit-reflog)
(global-set-key (kbd "C-x g t") 'magit-tag)
;; (eval-after-load "magit"
;;   '(define-key magit-status-mode-map (kbd "C-c C-a") 'magit-just-amend))
(define-key magit-status-mode-map (kbd "C-x C-k") 'magit-kill-file-on-line)
(define-key magit-status-mode-map (kbd "q") 'magit-quit-session)
(define-key magit-commit-mode-map (kbd "<tab>") 'company-complete)
(global-set-key (kbd "M-l") 'company-gtags)

;; woman-at-point
;; (global-set-key (kbd "C-c C-w") 'helm-man-woman)

;; add windmove to diff-mode
;; (defun my-diff-hook ()
;;   (local-set-key (kbd "M-i") 'windmove-up)
;;   (local-set-key (kbd "M-j") 'windmove-left)
;;   (local-set-key (kbd "M-l") 'windmove-right)
;;   (local-set-key (kbd "M-k") 'windmove-down)
;;   )

;; (add-hook 'diff-mode-hook 'my-diff-hook)

;; iedit
;; (global-set-key (kbd "C-c ;") 'iedit-mode)

;; vlf
;; rebinds default key
(eval-after-load "vlf"
  '(define-key vlf-prefix-map "\C-cv" vlf-mode-map))

;; visual-regexp
;; (define-key global-map (kbd "M-%") 'vr/replace)
(define-key global-map (kbd "M-%") 'vr/query-replace)
;; if you use multiple-cursors, this is for you:
(define-key global-map (kbd "C-c m") 'vr/mc-mark)

;; windmove
;; (global-set-key (kbd "C-c i") 'windmove-up)
;; (global-set-key (kbd "C-c j") 'windmove-left)
;; (global-set-key (kbd "C-c l") 'windmove-right)
;; (global-set-key (kbd "C-c k") 'windmove-down)

;; rebind cua-rectangle-mark
(setq cua-rectangle-mark-key (kbd "C-x <SPC>"))
(cua-mode 1)
(setq cua-auto-tabify-rectangles nil)
(setq cua-enable-cua-keys nil)           ;; don't add C-x,C-c,C-v

;; select windows with ido
;; (global-set-key (kbd "C-x o") 'ido-select-window)
;; (global-set-key (kbd "C-x o") 'ido-select-window)

;; ace-jump key bindings
(global-unset-key (kbd "C-c j"))
;; (global-set-key (kbd "C-x o") 'ace-window)
(global-set-key (kbd "C-c j") 'ace-jump-char-mode)

;; (global-set-key (kbd "C-c b") 'helm-buffers-list)

;; auto-complete
;; (global-set-key (kbd "<tab>") 'my-company-complete)
(define-key company-active-map (kbd "C-n") 'company-select-next)
(define-key company-active-map (kbd "C-p") 'company-select-previous)
(add-to-list 'hippie-expand-try-functions-list 'yas/hippie-try-expand)

(define-key magit-mode-map [(tab)] 'magit-toggle-section)
(define-key Info-mode-map [(tab)] 'Info-next-reference)

(define-key ac-complete-mode-map "\C-n" 'ac-next)
(define-key ac-complete-mode-map "\C-p" 'ac-previous)

(global-set-key (kbd "C-h s") 'sys-apropos)
(global-set-key (kbd "C-h C-s") 'find-function)
(global-set-key (kbd "C-h .") 'find-function-at-point)

(define-key ac-completing-map [return] 'ac-complete)
(global-set-key (kbd "C-:") 'ac-complete-with-helm)
;; (global-set-key (kbd "<tab>") 'ac-complete-with-helm)

(eval-after-load 'ztree-diff
  '(progn
     (define-key ztree-mode-map (kbd "<tab>") 'ztree-jump-side)
     (define-key ztree-mode-map (kbd "n") 'next-line)
     (define-key ztree-mode-map (kbd "p") 'previous-line)))

(add-hook 'makefile-mode-hook (lambda () (local-set-key (kbd "<tab>") 'indent-for-tab-command)))
(add-hook 'tabulated-list-mode-hook (lambda () (local-set-key (kbd "<tab>") 'forward-button)))
;; (define-key helm-map (kbd "M-l") 'helm-company-run-show-doc-buffer)
;; (define-key helm-map (kbd "M-j") 'helm-company-run-show-location)
;; (define-key helm-map (kbd "M-i") 'undefined)
;; (define-key helm-map (kbd "M-k") 'undefined)
;; (define-key helm-map (kbd "M-j") 'undefined)
;; (define-key helm-map (kbd "M-l") 'undefined)

;; helm
;; (global-unset-key (kbd "M-x"))
;; (global-set-key (kbd "C-x c g") 'helm-do-grep) ;;replaced with igrep
;; (global-set-key (kbd "C-M-/") 'helm-dabbrev)
;; (global-set-key (kbd "M-:") 'helm-eval-expression-with-eldoc)
(global-set-key (kbd "M-x") 'helm-M-x)
(global-set-key (kbd "M-y") 'helm-show-kill-ring)
(define-key projectile-mode-map (kbd "C-c p g") 'helm-git-grep)
(global-set-key (kbd "C-x C-b") 'helm-mini)
(global-set-key (kbd "C-x b") 'helm-mini)
(global-set-key (kbd "C-x C-f") 'helm-find-files)
;; (global-set-key (kbd "C-c C-m") 'helm-all-mark-rings)
;; (global-set-key (kbd "C-c f") 'helm-recentf)
;; (global-set-key (kbd "C-c h i") 'helm-imenu)
(global-set-key (kbd "C-c h m") 'helm-man-woman)
(global-set-key (kbd "C-c h f") 'helm-find)
;; (global-set-key (kbd "C-s") 'helm-occur)
(global-set-key (kbd "C-c h o") 'helm-occur)
(global-set-key (kbd "C-c h s") 'helm-swoop)
(global-set-key (kbd "C-c h k") 'helm-make)
;; (global-set-key (kbd "C-c C-f") 'prelude-recentf-ido-find-file)
;; (global-set-key (kbd "C-x y") 'helm-show-kill-ring)
;; (global-set-key (kbd "C-c h n") 'helm-mark-ring)
(global-set-key (kbd "C-c h M-g") 'helm-google-suggest)
(global-set-key (kbd "C-h r") 'helm-info-emacs)
(global-unset-key (kbd "C-h d"))
(global-set-key (kbd "C-h d d") 'helm-dictionary)
(global-set-key (kbd "C-h d s") 'dictionary-search)
(global-set-key (kbd "C-h d m") 'dictionary-match-words)
(global-set-key (kbd "C-h C-f") 'helm-apropos)
(global-set-key (kbd "C-c h y") 'helm-yas-complete)

(define-key helm-find-files-map (kbd "C-d") 'helm-ff-persistent-delete)
(define-key global-map [remap jump-to-register] 'helm-register)
(define-key global-map [remap list-buffers] 'helm-buffers-list)
(define-key global-map [remap find-tag] 'helm-gtags-select)

(define-key shell-mode-map (kbd "M-p") 'helm-comint-input-ring) ; shell history.
;;; Lisp complete or indent. (Rebind <tab>)
;;
(define-key lisp-interaction-mode-map [remap indent-for-tab-command] 'helm-lisp-completion-at-point)
(define-key emacs-lisp-mode-map [remap indent-for-tab-command] 'helm-lisp-completion-at-point)
;;; lisp complete. (Rebind M-<tab>)
;;
(define-key lisp-interaction-mode-map [remap completion-at-point] 'helm-lisp-completion-at-point)
(define-key emacs-lisp-mode-map [remap completion-at-point] 'helm-lisp-completion-at-point)
(unless (boundp 'completion-in-region-function)
  (add-hook 'ielm-mode-hook
            #'(lambda ()
                (define-key ielm-map [remap completion-at-point] 'helm-lisp-completion-at-point))))

;; helm-slime and slime key bindings
(dolist (m `(,slime-mode-map ,slime-repl-mode-map))
  (define-key m [(tab)] 'helm-slime-complete)
  (define-key m (kbd "C-c s a") 'helm-slime-apropos)
  (define-key m (kbd "C-c s h") 'helm-slime-repl-history)
  (define-key m (kbd "C-c s c") 'helm-slime-list-connections)
  (define-key m (kbd "C-c s u") 'helm-slime-update-connection-list)
  (define-key m (kbd "C-c s d") 'slime-hyperspec-lookup)
  (define-key m (kbd "C-c s p") 'slime-previous-note)
  (define-key m (kbd "C-c s n") 'slime-next-note)
  ;; (if (equalp m slime-mode-map)
  ;;     (progn
  ;;       (define-key m (kbd "M-n") 'highlight-symbol-next)
  ;;       (define-key m (kbd "M-p") 'highlight-symbol-prev)))
  )

;; (define-key slime-repl-mode-map [(tab)] 'helm-slime-complete)
;; (define-key slime-repl-mode-map (kbd "C-c s a") 'helm-slime-apropos)
;; (define-key slime-repl-mode-map (kbd "C-c s h") 'helm-slime-repl-history)
;; (define-key slime-repl-mode-map (kbd "C-c s c") 'helm-slime-list-connections)
;; (define-key slime-repl-mode-map (kbd "C-c s u") 'helm-slime-update-connection-list)
;; (define-key slime-repl-mode-map (kbd "C-c s d") 'slime-hyperspec-lookup)
;; (define-key slime-repl-mode-map (kbd "C-c s p") 'slime-previous-note)
;; (define-key slime-repl-mode-map (kbd "C-c s n") 'slime-next-note)
;; (define-key slime-repl-mode-map (kbd "M-p") 'highlight-symbol-next)
;; (define-key slime-repl-mode-map (kbd "M-n") 'highlight-symbol-prev)

;; ;; helm-gtags
;; (eval-after-load "helm-gtags"
;;   '(progn
;;      (define-key helm-gtags-mode-map (kbd "M-t") 'helm-gtags-find-tag)
;;      (define-key helm-gtags-mode-map (kbd "M-r") 'helm-gtags-find-rtag)
;;      (define-key helm-gtags-mode-map (kbd "M-s") 'helm-gtags-find-symbol)
;;      (define-key helm-gtags-mode-map (kbd "M-g M-p") 'helm-gtags-parse-file)
;;      (define-key helm-gtags-mode-map (kbd "C-c <") 'helm-gtags-previous-history)
;;      (define-key helm-gtags-mode-map (kbd "C-c >") 'helm-gtags-next-history)
;;      (define-key helm-gtags-mode-map (kbd "M-,") 'helm-gtags-pop-stack)))

(eval-after-load 'flycheck
  '(define-key flycheck-mode-map (kbd "C-c ! h") 'helm-flycheck))

;; (global-set-key (kbd "C-\\ s") 'save-current-configuration)
;; (global-set-key (kbd "C-\\ r") 'resume)
;; (global-set-key (kbd "C-\\ k") 'wipe)

;; Change the keybinds to whatever you like :)
;; (global-set-key (kbd  "C-c h b") 'helm-swoop-back-to-last-point)
(global-set-key (kbd "C-c h s") 'helm-swoop)

;; When doing isearch, hand the word over to helm-swoop
(define-key isearch-mode-map (kbd "M-i") 'helm-swoop-from-isearch)
;; Change the keybinds to whatever you like :)
(global-set-key (kbd "M-i") 'helm-swoop)
(global-set-key (kbd "M-I") 'helm-swoop-back-to-last-point)
;; (global-set-key (kbd "C-c M-") 'helm-multi-swoop)
